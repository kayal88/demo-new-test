package com.vinbytes.binary.services;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.authorization.client.util.Http;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@Service
public class KeyCloakService {

    @Value("${keycloak.auth-server-url}")
    private String authServerUrl;
    @Value("${keycloak.realm}")
    private String realm;
    @Value("${keycloak.resource}")
    private String clientId;
    @Value("${keycloak.credentials.secret}")
    private String clientSecret;


    @Value("${masterRealm}")
    private String masterRealm;

    @Value("${masterClientId}")
    private String masterClientId;

    @Value("${masterUsername}")
    private String masterUsername;

    @Value("${masterPassword}")
    private String masterPassword;

    public Response userSignup(String userName, String phoneNumber, String emailId, String password){

        Keycloak keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS).realm(realm).clientId(clientId).clientSecret(clientSecret)
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

        UserRepresentation keyCloakUser = new UserRepresentation();
        keyCloakUser.setEnabled(true);
        keyCloakUser.setUsername(phoneNumber);
        keyCloakUser.setFirstName(userName);
        keyCloakUser.setLastName(userName);
        keyCloakUser.setEmail(emailId);
        keyCloakUser.setEmailVerified(false);
        keyCloakUser.setGroups(new ArrayList<>());

        CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(false);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(password);
        keyCloakUser.setCredentials(Collections.singletonList(passwordCred));

        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();
        Response response = usersResource.create(keyCloakUser);
        if (response.getStatus()==201){
            System.out.println("success");
        }
        return response;
    }

    public Response userSignupForOauth(String userName, String phoneNumber, String emailId){

        Keycloak keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS).realm(realm).clientId(clientId).clientSecret(clientSecret)
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

        UserRepresentation keyCloakUser = new UserRepresentation();
        keyCloakUser.setEnabled(true);
        keyCloakUser.setUsername(phoneNumber);
        keyCloakUser.setFirstName(userName);
        keyCloakUser.setLastName(userName);
        keyCloakUser.setEmail(emailId);
        keyCloakUser.setEmailVerified(false);
        keyCloakUser.setGroups(new ArrayList<>());

        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();
        Response response = usersResource.create(keyCloakUser);
        if (response.getStatus()==201){
            System.out.println("success");
        }
        return response;
    }

    public AccessTokenResponse userLogin(String email, String password){
        Map<String, Object> clientCredentials = new HashMap<>();
        clientCredentials.put("secret", clientSecret);
        clientCredentials.put("grant_type", "password");

        Configuration configuration =
                new Configuration(authServerUrl, realm, clientId, clientCredentials, null);
        AuthzClient authzClient = AuthzClient.create(configuration);
        AccessTokenResponse response = new AccessTokenResponse();
        try {
            response = authzClient.obtainAccessToken(email, password);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return response;
    }

    public String getUserWithName(String userName){

        try {
            Keycloak keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl).realm(masterRealm)
                    .username(masterUsername) .password(masterPassword).clientId(masterClientId)
                    .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            List<UserRepresentation> userInfo = usersResource.search(userName,true);
            if (userInfo.size()==1){

                return userInfo.get(0).getEmail();
            }else {
                return "User Not Found !";
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
            return "User Not Found !";
        }
    }

    public AccessTokenResponse getAllUser(){

        Keycloak keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl).realm(masterRealm)
                .username(masterUsername) .password(masterPassword).clientId(masterClientId)
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();
        System.out.println(usersResource.search("kishor",true).get(0).getEmail());
//        Optional<UserRepresentation> userList = usersResource.list().stream().filter(usr -> usr.getUsername().equals(userName)).findAny();
//        System.out.println(usersResource.list());
        /*System.out.println(usersResource.search("user1"));*/
        /*List<UserRepresentation> AllUsr = usersResource.search("user1");
        System.out.println(usersResource.search("user1"));*/

        Map<String, Object> clientCredentials = new HashMap<>();
        clientCredentials.put("secret", clientSecret);
        clientCredentials.put("grant_type", "password");

        Configuration configuration =
                new Configuration(authServerUrl, realm, clientId, clientCredentials, null);
        String url = authServerUrl + "/admin/realms/" + realm + "/users";
        Http http = new Http(configuration, (params, headers) -> {});
        List<UserRepresentation> allUsers = new ArrayList<UserRepresentation>();
        AccessTokenResponse response = new AccessTokenResponse();
        try {
            /*http.<List<UserRepresentation>>get(url)
                    .authentication()
                    .client()
                    .form()
                    .response()
                    .execute();*/
            System.out.println(http.<List<UserRepresentation>>get(url)
                    .authentication()
                    .client()
                    .form()
                    .param("briefRepresentation", "true")
                    .response()
                    .execute());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return response;
    }

    public AccessTokenResponse TokenExchange(String authorizeCode){

        Map<String, Object> clientCredentials = new HashMap<>();
        clientCredentials.put("secret", clientSecret);
        clientCredentials.put("grant_type", "password");

        Configuration configuration =
                new Configuration(authServerUrl, realm, clientId, clientCredentials, null);
        String url = authServerUrl + "/realms/" + realm + "/protocol/openid-connect/token";
        Http http = new Http(configuration, (params, headers) -> {});
        AccessTokenResponse response = new AccessTokenResponse();
        try {
            response = http.<AccessTokenResponse>post(url)
                    .authentication()
                    .client()
                    .form()
                    .param("grant_type", "authorization_code")
                    .param("code", authorizeCode)
//                    .param("redirect_uri", "https://sandbox.play-crypto.com/login")
                    .param("redirect_uri", "http://localhost:4200/auth")
                    .param("client_id", clientId)
                    .param("client_secret", clientSecret)
                    .response()
                    .json(AccessTokenResponse.class)
                    .execute();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return response;
    }


    public AccessTokenResponse userTokenRefresh(String refreshToken){

        Map<String, Object> clientCredentials = new HashMap<>();
        clientCredentials.put("secret", clientSecret);
        clientCredentials.put("grant_type", "password");

        Configuration configuration =
                new Configuration(authServerUrl, realm, clientId, clientCredentials, null);
        String url = authServerUrl + "/realms/" + realm + "/protocol/openid-connect/token";
        Http http = new Http(configuration, (params, headers) -> {});
        AccessTokenResponse response = new AccessTokenResponse();
        try {
            response = http.<AccessTokenResponse>post(url)
                    .authentication()
                    .client()
                    .form()
                    .param("grant_type", "refresh_token")
                    .param("refresh_token", refreshToken)
                    .param("client_id", clientId)
                    .param("client_secret", clientSecret)
                    .response()
                    .json(AccessTokenResponse.class)
                    .execute();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return response;
    }


    public String userInfo(String Token){
        Object state = null;
        String url = authServerUrl + "/realms/" + realm + "/protocol/openid-connect/userinfo";
        try {
            URL url1 = new URL(url);
            HttpURLConnection con = (HttpURLConnection) url1.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization","Bearer "+Token);
            Reader streamReader = null;
            boolean Error = false;
            int status = con.getResponseCode();
            if (status > 299) {
                streamReader = new InputStreamReader(con.getErrorStream());
                Error = true;
            } else {
                streamReader = new InputStreamReader(con.getInputStream());
            }
            BufferedReader in = new BufferedReader(streamReader);
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            if (!Error){
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(String.valueOf(content));
//                System.out.println(json+"  1");
                        if(!json.isEmpty())
                        {
//                            System.out.println( json.get("email")+"  1datadata");
                            state= json.get("email");
                        }


            }



        }catch (Exception e){
            System.out.println(e.getMessage());          
        }

        return String.valueOf(state);

    }



    public void validToken(String refreshToken){

        Map<String, Object> clientCredentials = new HashMap<>();
        clientCredentials.put("secret", clientSecret);
        clientCredentials.put("grant_type", "password");

        Configuration configuration =
                new Configuration(authServerUrl, realm, clientId, clientCredentials, null);
        String url = authServerUrl + "/realms/" + realm + "/protocol/openid-connect/token/introspect";
        Http http = new Http(configuration, (params, headers) -> {});
        AccessTokenResponse response = new AccessTokenResponse();
        try {
            response = http.<AccessTokenResponse>post(url)
                    .authentication()
                    .client()
                    .form()
                    .param("token", refreshToken)
                    .param("client_id", clientId)
                    .param("client_secret", clientSecret)
                    .response()
                    .json(AccessTokenResponse.class)
                    .execute();

            System.out.println(response+"fromKeyCloak");

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
//        return response;
    }

    public boolean userUpdate(String userName, String password){

        boolean state = false;
        try {
            Keycloak keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl).realm(masterRealm)
                    .username(masterUsername) .password(masterPassword).clientId(masterClientId)
                    .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            List<UserRepresentation> userInfo = usersResource.search(userName,true);

            if (userInfo.size()==1){
                Response response;
                UserRepresentation singleUser = userInfo.get(0);
                usersResource.delete(singleUser.getId());
                if (userName.equals("")){
                    response = userSignup(singleUser.getFirstName(), singleUser.getUsername(), singleUser.getEmail(), password);
                }else {
                    response = userSignup(userName, singleUser.getUsername(), singleUser.getEmail(), password);
                }

                if (response.getStatus()==201){
                    state = true;
                }
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return state;
    }


    public boolean userUpdateEmail(String userName, String password, String email){

        boolean state = false;
        try {
            Keycloak keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl).realm(masterRealm)
                    .username(masterUsername) .password(masterPassword).clientId(masterClientId)
                    .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            List<UserRepresentation> userInfo = usersResource.search(userName,true);
            if (userInfo.size()==1){
                Response response;
                UserRepresentation singleUser = userInfo.get(0);
                usersResource.delete(singleUser.getId());
                if (userName.equals("")){
                    response = userSignup(singleUser.getFirstName(), singleUser.getUsername(), email, password);
                }else {
                    response = userSignup(userName, singleUser.getUsername(), email, password);
                }

                if (response.getStatus()==201){
                    state = true;
                }
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return state;
    }


    public boolean userUpdatePhone(String userName, String password, String phone){

        boolean state = false;
        try {
            Keycloak keycloak = KeycloakBuilder.builder().serverUrl(authServerUrl).realm(masterRealm)
                    .username(masterUsername) .password(masterPassword).clientId(masterClientId)
                    .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

            RealmResource realmResource = keycloak.realm(realm);
            UsersResource usersResource = realmResource.users();
            List<UserRepresentation> userInfo = usersResource.search(userName,true);
            if (userInfo.size()==1){
                Response response;
                UserRepresentation singleUser = userInfo.get(0);
                usersResource.delete(singleUser.getId());
                if (phone.equals("")){
                    response = userSignup(singleUser.getFirstName(), singleUser.getUsername(), singleUser.getEmail(), password);
                }else {
                    response = userSignup(singleUser.getFirstName(), phone, singleUser.getEmail(), password);
                }

                if (response.getStatus()==201){
                    state = true;
                }
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return state;
    }

}
