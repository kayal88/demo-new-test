package com.vinbytes.binary.services;

import com.vinbytes.binary.model.DynamicSlabModel;
import com.vinbytes.binary.model.DynamicSlabValues;
import com.vinbytes.binary.repository.DynamicSlabRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DynamicSlabService {

    @Autowired
    private DynamicSlabRepository dynamicSlabRepository;

    private static final Logger log = LoggerFactory.getLogger(DynamicSlabService.class);

    @EventListener(ApplicationReadyEvent.class)
    public void startUpInitializer(){
        /*List<String> typeList = Arrays.asList("3", "5", "15", "30");*/
        List<String> typeList = Arrays.asList("3", "5", "7");
        for (String type : typeList) {
            if (InitializeDynamicSlab(type)){
                log.info("Initialization of "+type+"min Slab Created Successfully");
            }else {
                log.error("Initialization of "+type+"min Slab not Created Successfully");
            }
        }
    }

    public boolean InitializeDynamicSlab(String type){
        try {
            DynamicSlabModel dynamicSlabModel = dynamicSlabRepository.findByType(type);
            if (dynamicSlabModel == null){
                dynamicSlabModel = new DynamicSlabModel();
                dynamicSlabModel.setType(type);
                List<Double> dynamicSlabDoubleValues = new ArrayList<>();
                /*switch (type){
                    case "3":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 5.00, 6.00, 10.00, 11.00, 15.00, 16.00, 20.00, 21.00, 25.00, 26.00, 30.00, 31.00, 35.00, 36.00, 40.00, 41.00, 41.00);
                        break;
                    case "5":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 10.00, 11.00, 20.00, 21.00, 30.00, 31.00, 40.00, 41.00, 50.00, 51.00, 60.00, 61.00, 70.00, 71.00, 80.00, 81.00, 81.00);
                        break;
                    case "7":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 10.00, 11.00, 20.00, 21.00, 30.00, 31.00, 40.00, 41.00, 50.00, 51.00, 60.00, 61.00, 70.00, 71.00, 80.00, 81.00, 81.00);
                        break;
                    case "15":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 25.00, 26.00, 50.00, 51.00, 75.00, 76.00, 100.00, 101.00, 125.00, 126.00, 150.00, 151.00, 175.00, 176.00, 200.00, 201.00, 201.00);
                        break;
                    case "30":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 50.00, 51.00, 100.00, 101.00, 150.00, 151.00, 200.00, 201.00, 250.00, 251.00, 300.00, 301.00, 350.00, 351.00, 400.00, 401.00, 401.00);
                        break;
                }*/
                switch (type){
                    case "3":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 15.00, 16.00, 30.00, 31.00, 45.00, 46.00, 46.00);
                        break;
                    case "5":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 30.00, 31.00, 60.00, 61.00, 80.00, 81.00,81.00);
                        break;
                    case "7":
                        dynamicSlabDoubleValues = Arrays.asList(0.00, 35.00, 41.00, 60.00, 61.00, 90.00, 91.00, 91.00);
                        break;
                }
                List<DynamicSlabValues> dynamicSlabValuesList = new ArrayList<>();
                for (int i = 0; i < 8; i=i+2) {
                    DynamicSlabValues dynamicSlabValues = dynamicSlabValuesGenerator(dynamicSlabDoubleValues.get(i), dynamicSlabDoubleValues.get(i+1));
                    dynamicSlabValuesList.add(dynamicSlabValues);
                }
                dynamicSlabModel.setDynamicSlabValues(dynamicSlabValuesList);
                dynamicSlabRepository.save(dynamicSlabModel);
            }
            return true;
        }catch (Exception e){
            log.error(e.getMessage());
            return false;
        }
    }

    public DynamicSlabValues dynamicSlabValuesGenerator(Double from, Double to){
        DynamicSlabValues dynamicSlabValues = new DynamicSlabValues();
        dynamicSlabValues.setFromValue(from);
        dynamicSlabValues.setToValue(to);
        return dynamicSlabValues;
    }
}
