package com.vinbytes.binary.services;

import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.TransactionRepository;
import com.vinbytes.binary.repository.UserRepository;
import com.vinbytes.binary.repository.WalletRepository;
import org.apache.tomcat.util.codec.binary.Base64;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.*;

@Service
public class walletServices {

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationService notificationService;

    @Value("${node-coinPayments-url}")
    private String nodeCoinPaymentsUrl;


    private static final Logger log = LoggerFactory.getLogger(walletServices.class);

    public coinPaymentsDepositResponseModel walletDepositRequest(addMoneyRequestModel addMoneyRequestModel){
        try{
            RestTemplate restTemplate = new RestTemplate();
            String requestUrl = nodeCoinPaymentsUrl + "/deposit";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<addMoneyRequestModel> requestBody = new HttpEntity<>(addMoneyRequestModel, headers);
            return restTemplate.postForObject(requestUrl, requestBody, coinPaymentsDepositResponseModel.class);
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return null;
        }
    }

    public coinPaymentsWithdrawalResponseModel walletWithdrawalRequest(withdrawalModel withdrawalModel){
        try{
            RestTemplate restTemplate = new RestTemplate();
            String requestUrl = nodeCoinPaymentsUrl + "/withdrawal";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<withdrawalModel> requestBody = new HttpEntity<>(withdrawalModel ,headers);
            return restTemplate.postForObject(requestUrl, requestBody, coinPaymentsWithdrawalResponseModel.class);
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return null;
        }
    }

    public boolean initialWalletCreationForUser(String userId){
        try{
            log.info("Inside walletServices.initialWalletCreationForUser()");
            UserModel userFromDB = userRepository.findById(userId).orElse(null);
            if (userFromDB != null){
                if (userFromDB.isWalletInitialization()){
                    log.warn("user wallet initialization already completed !!");
                    return false;
                }else {
                    WalletModel walletModel = walletRepository.findByUserId(userFromDB.getId());
                    if (walletModel != null){
                        log.warn("user wallet initialization already completed !!");
                        return false;
                    }else {
                        walletModel = new WalletModel();
                        walletModel.setUserId(userFromDB.getId());
                        walletRepository.save(walletModel);

                        walletModel = walletRepository.findByUserId(userFromDB.getId());
                        if (walletModel == null){
                            log.error("wallet init Error !");
                            return false;
                        }else {
                            userFromDB.setWalletId(walletModel.getId());
                            userFromDB.setWalletInitialization(true);
                            userRepository.save(userFromDB);
                            log.info("user wallet initialization was successful !!");
                            return true;
                        }
                    }
                }
            }else {
                log.warn("user not found !!");
                return false;
            }
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }

    public boolean walletDepositInit(String userId, coinPaymentsDepositResponseModel coinPaymentsDepositResponseModel){
        try{
            WalletModel walletModel = walletRepository.findByUserId(userId);
            if (walletModel == null){
                log.error("wallet Deposit init Error !");
                return false;
            }else {
                coinPaymentsDepositResponseModel.setTxnId(coinPaymentsDepositResponseModel.getTxn_id());
                transactionStatusModel transactionStatusModel = new transactionStatusModel(new Date(), "Initiated", 0.00);
                List<transactionStatusModel> transactionStatusModelList = new ArrayList<>();
                transactionStatusModelList.add(transactionStatusModel);
                Date ExpireDate = new Date();
                ExpireDate.setHours(ExpireDate.getHours()+1);
                TransactionModal transactionModal = new TransactionModal();
                transactionModal.setWalletId(walletModel.getId());
                transactionModal.setWalletType("Deposit Wallet");
                transactionModal.setTransactionId(randomTransactionIdGenerator("deposit"));
                transactionModal.setTransactionAddress(coinPaymentsDepositResponseModel.getAddress());
                transactionModal.setCoinBaseTransactionId(coinPaymentsDepositResponseModel.getTxnId());
                transactionModal.setCoinBaseTransactionHostedUrl(coinPaymentsDepositResponseModel.getCheckout_url());
                transactionModal.setCoinBaseTransactionHostedUrlExpires(ExpireDate);
                transactionModal.setTransactionInitiateDateAndTime(new Date());
                transactionModal.setTotalAmount(coinPaymentsDepositResponseModel.getAmount());
                transactionModal.setUserId(userId);
                transactionModal.setTransactionMode("Coin Payments");
                transactionModal.setTransactionType("Wallet Topup");
                transactionModal.setTransactionStatus("Initiated");
                transactionModal.setCoinPaymentsDepositResponse(coinPaymentsDepositResponseModel);
                transactionModal.setTransactionStatusList(transactionStatusModelList);
                transactionModal.setTransactionState(true);
                transactionRepository.save(transactionModal);
                log.info("wallet Deposit init Successful !!");
                return true;
            }
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }

    public boolean walletDepositPayment(TransactionModal transactionModel){
        try{
            WalletModel walletModel = walletRepository.findById(transactionModel.getWalletId()).orElse(null);
            if (walletModel == null){
                log.error("wallet Deposit init Error !");
                return false;
            }else {
                Double receivedAmount = transactionModel.getReceivedAmount();
                walletModel.setUserId(transactionModel.getUserId());
                walletModel.setDollars(walletModel.getDollars() + receivedAmount);
                walletModel.setTotalCredit(walletModel.getTotalCredit() + receivedAmount);
                walletModel.setTotalNumberOfTimesCredit(walletModel.getTotalNumberOfTimesCredit() + 1);
                walletModel.setTotalTransactionFee(walletModel.getTotalTransactionFee() + transactionModel.getTransactionFeeAmount());
                /*notificationService.MakeNotifications(transactionModel.getUserId(), "Your Deposit of "+receivedAmount+" was Successful.", "GET!SET!GO!!!", transactionModel.getCoinBaseTransactionHostedUrl(), "/wallet");*/
                walletRepository.save(walletModel);
                log.info("wallet Deposit payment Successful !!");
                return true;
            }
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }

    public boolean walletWithdrawalInit(String userId, coinPaymentsWithdrawalResponseModel coinPaymentsWithdrawalResponseModel, withdrawalModel withdrawalModel){
        try{
            WalletModel walletModel = walletRepository.findByUserId(userId);
            if (walletModel == null){
                log.error("wallet withdrawal init Error !");
                return false;
            }else {
                Double debitedAmount = coinPaymentsWithdrawalResponseModel.getAmount();
                String transactionId = randomTransactionIdGenerator("withdrawal");
                walletModel.setDollars(walletModel.getDollars() - debitedAmount);
                walletModel.setTotalDebit(walletModel.getTotalDebit() + debitedAmount);
                walletModel.setTotalNumberOfTimesDebit(walletModel.getTotalNumberOfTimesDebit() + 1);
                walletRepository.save(walletModel);
                Date ExpireDate = new Date();
                ExpireDate.setHours(ExpireDate.getHours()+1);
                transactionStatusModel transactionStatusModel = new transactionStatusModel(new Date(), "Initiated", 0.00);
                List<transactionStatusModel> transactionStatusModelList = new ArrayList<>();
                transactionStatusModelList.add(transactionStatusModel);
                TransactionModal transactionModal = new TransactionModal();
                transactionModal.setWalletId(walletModel.getId());
                transactionModal.setWalletType("Withdrawal Wallet");
                transactionModal.setTransactionId(transactionId);
                transactionModal.setTransactionAddress(withdrawalModel.getAddress());
                transactionModal.setTransactionInitiateDateAndTime(new Date());
                transactionModal.setTotalAmount(coinPaymentsWithdrawalResponseModel.getAmount());
                transactionModal.setUserId(userId);
                transactionModal.setTransactionMode("Coin Payments");
                transactionModal.setTransactionType("Withdrawal Amount");
                transactionModal.setTransactionStatus("Initiated");
                transactionModal.setCoinPaymentsWithdrawalResponse(coinPaymentsWithdrawalResponseModel);
                transactionModal.setTransactionStatusList(transactionStatusModelList);
                transactionModal.setTransactionState(true);
                transactionRepository.save(transactionModal);
                log.info("wallet withdrawal init Successful !!");
                return true;
            }
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }

    public boolean coinPaymentsSecurityCheck(String merchantId, String Hmac, String requestPayLoad){
        try {
            String MerchantId = "04c36dfbefa22b24dfc5148f0b00d186";
            String secret = "profitridellc@gmail.com$newCAS8899";
            Mac sha512_HMAC = Mac.getInstance("HmacSHA512");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA512");
            sha512_HMAC.init(secret_key);
            String hash = Base64.encodeBase64String(sha512_HMAC.doFinal(requestPayLoad.getBytes()));
            return true;
        }catch (Exception e){
            log.error("unhandled Error Occurred : " + e.getMessage());
            return false;
        }
    }

    public String randomTransactionIdGenerator(String type){
        String randomTransactionId;
        String preStringForTransactionId;
        Date CurrentServerDate = new Date();
        Random r = new Random(System.currentTimeMillis());
        switch (type){
            case "credit":
                preStringForTransactionId = "CRD";
                break;
            case "debit":
                preStringForTransactionId = "DBT";
                break;
            case "deposit":
                preStringForTransactionId = "DP";
                break;
            case "withdrawal":
                preStringForTransactionId = "WDL";
                break;
            default:
                preStringForTransactionId = "OT";
        }
        randomTransactionId = preStringForTransactionId + CurrentServerDate.getYear() + CurrentServerDate.getMonth() + CurrentServerDate.getDate() + ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
        return randomTransactionId;
    }
}
