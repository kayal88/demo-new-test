package com.vinbytes.binary.services;

import com.vinbytes.binary.controller.TransactionController;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.*;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static java.lang.Long.parseLong;

@Service
public class CoinBasePaymentServices {

    @Autowired
    private PlayBidRepository playBidRepository;

    @Autowired
    private PurchaseBidsRepository purchaseBidsRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private OverallProgressRepository overallProgressRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CoinBaseTransactionRepository coinBaseTransactionRepository;

    @Autowired
    private PasswordEncoder encoder;

    private static final Logger log = LoggerFactory.getLogger(TransactionController.class);

    public void UNDERPAID(CoinBaseTransactionModal SingleCoinBaseTransactionModal, JSONObject timelineLastObject)
    {
        try
        {
            SingleCoinBaseTransactionModal.setTransactionStatus(timelineLastObject.get("status").toString());
            SingleCoinBaseTransactionModal.setReceivedPayment(true);
            SingleCoinBaseTransactionModal.setConfirmReceivedPayment(encoder.encode("true"));
            PurchaseBidsModel purchaseFrmDb=purchaseBidsRepository.findByTransactionId(SingleCoinBaseTransactionModal.getCoinBaseTransactionId());
            purchaseFrmDb.setStatus("Payment Failed");
            purchaseFrmDb.setPurchaseStatus("UNDERPAID");
            JSONObject payment = (JSONObject) timelineLastObject.get("payment");
            JSONObject BTCValue = (JSONObject) payment.get("value");
            TransactionModal transactionModal=transactionRepository.findByTransactionId(purchaseFrmDb.getTransactionId());
            transactionModal.setReceivedAmountFromCoinBase(Double.parseDouble(BTCValue.get("amount").toString()));
            transactionModal.setTransactionStatus("Payment Failed");
            purchaseBidsRepository.save(purchaseFrmDb);
            coinBaseTransactionRepository.save(SingleCoinBaseTransactionModal);

        }
        catch (Exception e)
        {
            log.info("Error in getting UNDERPAID.");
                System.out.print(e.getMessage());
        }

    }


    public void COMPLETED(CoinBaseTransactionModal SingleCoinBaseTransactionModal, JSONObject timelineLastObject)
    {

        try {
            SingleCoinBaseTransactionModal.setTransactionStatus(timelineLastObject.get("status").toString());
            SingleCoinBaseTransactionModal.setReceivedPayment(true);
            DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date Received = formatter1.parse((String) timelineLastObject.get("time"));
            SingleCoinBaseTransactionModal.setPaymentReceivedAt(Received);
            SingleCoinBaseTransactionModal.setConfirmReceivedPayment(encoder.encode("true"));
            PlayBidModel frmDb = playBidRepository.findByBidId((SingleCoinBaseTransactionModal.getStockId()));
            PurchaseBidsModel purchaseFrmDb = purchaseBidsRepository.findByTransactionId(SingleCoinBaseTransactionModal.getTransactionId());
            System.out.println(frmDb.getBiddingStatus().equals("Completed"));
            if (frmDb.getBiddingStatus().equals("Completed")) {
                TransactionModal transactionModal = transactionRepository.findByTransactionId(purchaseFrmDb.getTransactionId());
                transactionModal.setTransactionStatus("Refund Initiated");
                transactionRepository.save(transactionModal);
            } else {

                List<Double> ArrayOfIntForFindingMaxValue = new ArrayList<Double>(Collections.nCopies(18, 0.00));
                if (frmDb.getTotalNoOfAmountInSlab() != null && frmDb.getTotalNoOfAmountInSlab().size() != 0) {
                    ArrayOfIntForFindingMaxValue = frmDb.getTotalNoOfAmountInSlab();
                }

                String level = purchaseFrmDb.getLevel();
                int whichOneOfTheSlab = 0;
                switch (purchaseFrmDb.getChangesBtw()) {
                    case "$0 - $50":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 0;
                        } else {
                            whichOneOfTheSlab = 1;
                        }
                        break;
                    case "$51 - $100":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 2;
                        } else {
                            whichOneOfTheSlab = 3;
                        }
                        break;
                    case "$101 - $150":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 4;
                        } else {
                            whichOneOfTheSlab = 5;
                        }
                        break;
                    case "$151 - $200":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 6;
                        } else {
                            whichOneOfTheSlab = 7;
                        }
                        break;
                    case "$201 - $250":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 8;
                        } else {
                            whichOneOfTheSlab = 9;
                        }
                        break;
                    case "$251 - $300":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 10;
                        } else {
                            whichOneOfTheSlab = 11;
                        }
                        break;
                    case "$301 - $350":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 12;
                        } else {
                            whichOneOfTheSlab = 13;
                        }
                        break;
                    case "$351 - $400":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 14;
                        } else {
                            whichOneOfTheSlab = 15;
                        }
                        break;
                    case "over $400":
                        if (level.equals("Up")) {
                            whichOneOfTheSlab = 16;
                        } else {
                            whichOneOfTheSlab = 17;
                        }
                        break;

                    default:
                        System.out.println("Error, Changes Between -  Default statement !");
                        break;
                }

                purchaseFrmDb.setStatus("Success");
                purchaseFrmDb.setPurchaseStatus("Success");

                double ValueToBeAdded = 0.00;
                if (ArrayOfIntForFindingMaxValue.size() > 0) {

                    if (ArrayOfIntForFindingMaxValue.get(whichOneOfTheSlab) != null) {
                        ValueToBeAdded = ArrayOfIntForFindingMaxValue.get(whichOneOfTheSlab) + Double.parseDouble(purchaseFrmDb.getBiddingAmount());

                    } else {
                        ValueToBeAdded = Double.parseDouble(purchaseFrmDb.getBiddingAmount());
                    }

                    ArrayOfIntForFindingMaxValue.set(whichOneOfTheSlab, ValueToBeAdded);

                } else {
                    System.out.println("Error, Missing");
                }
                frmDb.setTotalNoOfAmountInSlab(ArrayOfIntForFindingMaxValue);

                if (frmDb.getTotalAmountBid() == 0) {
                    frmDb.setTotalAmountBid((Double.parseDouble(SingleCoinBaseTransactionModal.getUnitsPurchased())) * (Double.parseDouble(SingleCoinBaseTransactionModal.getValueOfOneBid())));
                } else {
                    frmDb.setTotalAmountBid(frmDb.getTotalAmountBid() + ((Double.parseDouble(SingleCoinBaseTransactionModal.getUnitsPurchased())) * (Double.parseDouble(SingleCoinBaseTransactionModal.getValueOfOneBid()))));
                }

                if (frmDb.getUserId() == null) {
                    frmDb.setUserId(Collections.singletonList(purchaseFrmDb.getUserId()));
                } else {
                    List<String> userId = frmDb.getUserId();
                    userId.add(purchaseFrmDb.getUserId());
                    frmDb.setUserId(userId);
                }
                System.out.println((frmDb.getTotalTransactionFees())+Double.parseDouble(purchaseFrmDb.getTransactionFee()));
                    frmDb.setTotalTransactionFees((frmDb.getTotalTransactionFees())+Double.parseDouble(purchaseFrmDb.getTransactionFee()));
                frmDb.setTotalRevenue(frmDb.getTotalRevenue()+purchaseFrmDb.getTotalAmount());
                TransactionModal transactionModal = transactionRepository.findByTransactionId(purchaseFrmDb.getTransactionId());
                transactionModal.setTransactionStatus("Payment Success");
                OverallProgressModal overallProgressModal = overallProgressRepository.findAll().get(0);
                CoinBaseTransactionModal coinBaseFrmDb = coinBaseTransactionRepository.findByCoinBaseTransactionId(purchaseFrmDb.getCoinBasePurchaseId());
                if (coinBaseFrmDb.getTransactionStatus().equals("COMPLETED")) {
                    overallProgressModal.setTotalAmountCollectedForBids(overallProgressModal.getTotalAmountCollectedForBids() + Double.parseDouble(coinBaseFrmDb.getBiddingAmount()));
                    overallProgressModal.setTotalRevenueGenerated(overallProgressModal.getTotalRevenueGenerated() + coinBaseFrmDb.getTotalAmount());
                    overallProgressModal.setTotalAmountOfTransactionFeeForBidsPurchased(overallProgressModal.getTotalAmountOfTransactionFeeForBidsPurchased() + Double.parseDouble(coinBaseFrmDb.getTransactionFee()));

                }
                transactionRepository.save(transactionModal);
                coinBaseTransactionRepository.save(SingleCoinBaseTransactionModal);
                overallProgressRepository.save(overallProgressModal);
                purchaseBidsRepository.save(purchaseFrmDb);
                playBidRepository.save(frmDb);

            }
        }catch (Exception e)
        {
            log.info("Error in getting COMPLETED.");
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
        }
    }

    public void EXPIRED(CoinBaseTransactionModal SingleCoinBaseTransactionModal, JSONObject timelineLastObject) {

        try {
            SingleCoinBaseTransactionModal.setTransactionStatus(timelineLastObject.get("status").toString());
            SingleCoinBaseTransactionModal.setReceivedPayment(true);
            SingleCoinBaseTransactionModal.setConfirmReceivedPayment(encoder.encode("true"));
            PurchaseBidsModel purchaseFrmDb=purchaseBidsRepository.findByTransactionId(SingleCoinBaseTransactionModal.getCoinBaseTransactionId());
            purchaseFrmDb.setStatus("Payment Failed");
            purchaseFrmDb.setPurchaseStatus("EXPIRED");
            purchaseBidsRepository.save(purchaseFrmDb);
            coinBaseTransactionRepository.save(SingleCoinBaseTransactionModal);
        }catch (Exception e)
        {
            log.info("Error in getting EXPIRED.");
            System.out.println(e.getMessage());
        }


    }
}
