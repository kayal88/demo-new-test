package com.vinbytes.binary.services;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

@Service
public class AmazonSimpleNotificationService {

    private SnsClient getSnsClient() {

        AwsBasicCredentials awsBasicCredentials = AwsBasicCredentials.create("AKIA4MBZVSZKQOKWX7XS", "qheUW1pkX2sUOPp/hpgYPih9E//aHpBsetJQWWto");
        AwsCredentialsProvider awsCredentialsProvider = () -> awsBasicCredentials;
        return SnsClient.builder()
                .credentialsProvider(awsCredentialsProvider)
                .region(Region.US_EAST_1)
                .build();
    }

    public void sendMessage(){

        try {
            String phone = "+919884481314";
            SnsClient snsClient = getSnsClient();

            final PublishRequest publishRequest = PublishRequest.builder()
                    .phoneNumber(phone)
                    .message("Test Message From Spring Boot - Your Play-Crypto verification code is: 8091326. Don't share this code anyone; our employees will never ask for the code.")
                    .build();

            PublishResponse publishResponse = snsClient.publish(publishRequest);

            if (publishResponse.sdkHttpResponse().isSuccessful()) {
                System.out.println("Message publishing to phone successful");
            } else {
                System.out.println("Message Not delivered !");
                throw new ResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR, publishResponse.sdkHttpResponse().statusText().get()
                );
            }
            snsClient.close();
        }catch (Exception e){
            System.out.println("Unknown Error Occurred. Message Not delivered ! Error: "+ e.getMessage());
        }
    }
}
