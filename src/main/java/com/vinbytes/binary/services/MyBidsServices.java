package com.vinbytes.binary.services;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.MyBidsModel;
import com.vinbytes.binary.repository.MyBidsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

public class MyBidsServices {
    @Autowired
    public MyBidsRepository myBidsRepository;

    public ResponseEntity<MessageResponse> createMyBids(MyBidsModel myBidsModel) {


        if (myBidsModel.getBidID() == null || myBidsModel.getStockName() == null || myBidsModel.getBiddingStartsAt() == null || myBidsModel.getBiddingClosesAt() == null || myBidsModel.getDrawTime() == null || myBidsModel.getStockValue() == null || myBidsModel.getAmountInvested() == null || myBidsModel.getChange() == null || myBidsModel.getBiddingStatus() == null || myBidsModel.getBiddingDate() == null) {
        //  Check whether all values were given by REQUEST
//        if (myBidsModel.getBidID() == null || myBidsModel.getStockName() == null || myBidsModel.getBiddingStartsAt() == null || myBidsModel.getBiddingDate() == null) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Some values are missing"));
        }
        try {
            //  Get all values from model and store it in Object(playBidModel)
            myBidsModel.setBidID(myBidsModel.getBidID());
            myBidsModel.setStockName(myBidsModel.getStockName());
            myBidsModel.setBiddingStartsAt(myBidsModel.getBiddingStartsAt());
            myBidsModel.setBiddingClosesAt(myBidsModel.getBiddingClosesAt());
            myBidsModel.setDrawTime(myBidsModel.getDrawTime());
            myBidsModel.setStockValue(myBidsModel.getStockValue());
            myBidsModel.setAmountInvested(myBidsModel.getAmountInvested());
            myBidsModel.setChange(myBidsModel.getChange());
            myBidsModel.setBiddingStatus(myBidsModel.getBiddingStatus());
            myBidsModel.setBiddingDate(myBidsModel.getBiddingDate());
//            playBidModel.setBidID(playBidModel.getBidID());
//            playBidModel.setStockName(playBidModel.getStockName());
//            playBidModel.setBiddingStartsAt(playBidModel.getBiddingStartsAt());
//            playBidModel.setBiddingClosesAt(playBidModel.getBiddingClosesAt());
//            playBidModel.setDrawTime(playBidModel.getDrawTime());
//            playBidModel.setStockValue(playBidModel.getStockValue());
//            playBidModel.setValueOfOneBid(playBidModel.getValueOfOneBid());
//            playBidModel.setNoOfBidders(playBidModel.getNoOfBidders());
//            playBidModel.setBiddingStatus(playBidModel.getBiddingStatus());
            myBidsRepository.save(myBidsModel);
            return ResponseEntity.ok(new MessageResponse("Play Bid - Bidding Details Created successfully!"));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Bad Request"));
        }

    }
}
