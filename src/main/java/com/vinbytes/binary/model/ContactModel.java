package com.vinbytes.binary.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contact")
public class ContactModel {

    private String name;
    private String email;
    private long phone;
    private String description;

    public ContactModel() {
    }

    public ContactModel(String name, String email, long phone, String description) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
