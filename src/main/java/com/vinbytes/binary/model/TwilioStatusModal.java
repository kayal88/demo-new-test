package com.vinbytes.binary.model;

public class TwilioStatusModal {

    private String status;
    private String payee;
    private String date_updated;
    private String account_sid;
    private String to;
    private String amount;
    private boolean valid;
    private String sid;
    private String date_created;
    private String service_sid;
    private String channel;

    public TwilioStatusModal() {
    }

    public TwilioStatusModal(String status, String payee, String date_updated, String account_sid, String to, String amount, boolean valid, String sid, String date_created, String service_sid, String channel) {
        this.status = status;
        this.payee = payee;
        this.date_updated = date_updated;
        this.account_sid = account_sid;
        this.to = to;
        this.amount = amount;
        this.valid = valid;
        this.sid = sid;
        this.date_created = date_created;
        this.service_sid = service_sid;
        this.channel = channel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(String date_updated) {
        this.date_updated = date_updated;
    }

    public String getAccount_sid() {
        return account_sid;
    }

    public void setAccount_sid(String account_sid) {
        this.account_sid = account_sid;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getService_sid() {
        return service_sid;
    }

    public void setService_sid(String service_sid) {
        this.service_sid = service_sid;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
