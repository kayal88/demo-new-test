package com.vinbytes.binary.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "DynamicSlab")
public class DynamicSlabModel {

    @Id
    private String id;
    private String type;
    private List<DynamicSlabValues> dynamicSlabValues;

    public DynamicSlabModel() {
    }

    public DynamicSlabModel(String id, String type, List<DynamicSlabValues> dynamicSlabValues) {
        this.id = id;
        this.type = type;
        this.dynamicSlabValues = dynamicSlabValues;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DynamicSlabValues> getDynamicSlabValues() {
        return dynamicSlabValues;
    }

    public void setDynamicSlabValues(List<DynamicSlabValues> dynamicSlabValues) {
        this.dynamicSlabValues = dynamicSlabValues;
    }
}
