package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@ApiModel(description = "Permissions Model")
@Document(collection = "Permissions")
public class PermissionModel {

    @Id
    @ApiModelProperty(value = "Permissions id", required = true)
    public String id;

    @ApiModelProperty(value = "user id", required = true)
    public String userId;
    @ApiModelProperty(value = "List of Permission Category Model", required = true)
    public List<PermissionCategoryModel> permissions;

    public PermissionModel() {
    }

    public PermissionModel(String id, String userId, List<PermissionCategoryModel> permissions) {
        this.id = id;
        this.userId = userId;
        this.permissions = permissions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<PermissionCategoryModel> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionCategoryModel> permissions) {
        this.permissions = permissions;
    }
}
