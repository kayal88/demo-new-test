package com.vinbytes.binary.model;

public class PermitNotificationModel {

    public boolean email;
    public boolean phone;
    public boolean push;

    public PermitNotificationModel() {
    }

    public PermitNotificationModel(boolean email, boolean phone, boolean push) {
        this.email = email;
        this.phone = phone;
        this.push = push;
    }

    public boolean getEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isPhone() {
        return phone;
    }

    public void setPhone(boolean phone) {
        this.phone = phone;
    }

    public boolean isPush() {
        return push;
    }

    public void setPush(boolean push) {
        this.push = push;
    }
}
