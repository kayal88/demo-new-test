package com.vinbytes.binary.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.io.Serializable;
import java.util.List;


@Document(collection = "Notifications")

public class NotificationModel implements Serializable{


    @Id
    private String id;
    private String userId;
    private List<NotificationDateFilter> notificationDateFilters;

    public NotificationModel() {
    }

    public NotificationModel(String id, String userId, List<NotificationDateFilter> notificationDateFilters) {
        this.id = id;
        this.userId = userId;
        this.notificationDateFilters = notificationDateFilters;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<NotificationDateFilter> getNotificationDateFilters() {
        return notificationDateFilters;
    }

    public void setNotificationDateFilters(List<NotificationDateFilter> notificationDateFilters) {
        this.notificationDateFilters = notificationDateFilters;
    }
}
