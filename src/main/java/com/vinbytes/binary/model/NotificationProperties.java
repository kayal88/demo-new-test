package com.vinbytes.binary.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NotificationProperties {

    private String title;
    private String description;
    private String image;
    private Date dateAndTime;
    private String url;
    private String transactionId;
    private boolean checked=false;
}
