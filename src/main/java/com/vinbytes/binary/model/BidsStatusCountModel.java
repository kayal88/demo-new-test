package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@ApiModel(description = "User Favourite Stocks")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BidsStatusCountModel {

    private Long OpeningSoon;
    private Long OpenNow;
    private Long Closed;
    private Long Completed;
    private Long Cancelled;

}
