package com.vinbytes.binary.model;

public class coinPaymentsDepositResponseModel {

    private Double amount;
    private String txn_id;
    private String txnId;
    private String address;
    private String confirms_needed;
    private long timeout;
    private String checkout_url;
    private String status_url;
    private String qrcode_url;

    public coinPaymentsDepositResponseModel() {
    }

    public coinPaymentsDepositResponseModel(Double amount, String txn_id, String txnId, String address, String confirms_needed, long timeout, String checkout_url, String status_url, String qrcode_url) {
        this.amount = amount;
        this.txn_id = txn_id;
        this.txnId = txnId;
        this.address = address;
        this.confirms_needed = confirms_needed;
        this.timeout = timeout;
        this.checkout_url = checkout_url;
        this.status_url = status_url;
        this.qrcode_url = qrcode_url;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConfirms_needed() {
        return confirms_needed;
    }

    public void setConfirms_needed(String confirms_needed) {
        this.confirms_needed = confirms_needed;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public String getCheckout_url() {
        return checkout_url;
    }

    public void setCheckout_url(String checkout_url) {
        this.checkout_url = checkout_url;
    }

    public String getStatus_url() {
        return status_url;
    }

    public void setStatus_url(String status_url) {
        this.status_url = status_url;
    }

    public String getQrcode_url() {
        return qrcode_url;
    }

    public void setQrcode_url(String qrcode_url) {
        this.qrcode_url = qrcode_url;
    }
}
