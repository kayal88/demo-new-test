package com.vinbytes.binary.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@ApiModel(description = "Notify Me Details")

@Document(collection = "notify")
public class NotifyMeModel {
    @Id
    @ApiModelProperty(value = "PK of Notify Model", required = true)
    private String id;

    @ApiModelProperty(value = "Device of Notify Model", required = true)
    private String device;

    @Email
    @ApiModelProperty(value = "Email of Notify Model", required = true)
    private String email;

    @ApiModelProperty(value = "LocalDateTime of Notify Model", required = true)
    private LocalDateTime emailEnteredTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getEmailEnteredTime() {
        return emailEnteredTime;
    }

    public void setEmailEnteredTime(LocalDateTime emailEnteredTime) {
        this.emailEnteredTime = emailEnteredTime;
    }
}