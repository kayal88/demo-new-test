package com.vinbytes.binary.model;


import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "helpDesk")
public class HelpDeskModel {

    private String email;
    private String subject;
    private String description;

    public HelpDeskModel() {
    }

    public HelpDeskModel(String email, String subject, String description) {
        this.email = email;
        this.subject = subject;
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
