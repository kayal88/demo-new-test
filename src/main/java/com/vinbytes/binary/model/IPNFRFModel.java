package com.vinbytes.binary.model;

import org.springframework.data.annotation.Id;

public class IPNFRFModel {

    @Id
    private String id;
    private double amount1;
    private double amount2;
    private String buyer_name;
    private String currency1;
    private String currency2;
    private String email;
    private double fee;
    private String ipn_id;
    private String ipn_mode;
    private String ipn_type;
    private float ipn_version;
    private String merchant;
    private double received_amount;
    private double received_confirms;
    private long status;
    private String status_text;
    private String txn_id;

    public IPNFRFModel() {
    }

    public IPNFRFModel(String id, double amount1, double amount2, String buyer_name, String currency1, String currency2, String email, double fee, String ipn_id, String ipn_mode, String ipn_type, float ipn_version, String merchant, double received_amount, double received_confirms, long status, String status_text, String txn_id) {
        this.id = id;
        this.amount1 = amount1;
        this.amount2 = amount2;
        this.buyer_name = buyer_name;
        this.currency1 = currency1;
        this.currency2 = currency2;
        this.email = email;
        this.fee = fee;
        this.ipn_id = ipn_id;
        this.ipn_mode = ipn_mode;
        this.ipn_type = ipn_type;
        this.ipn_version = ipn_version;
        this.merchant = merchant;
        this.received_amount = received_amount;
        this.received_confirms = received_confirms;
        this.status = status;
        this.status_text = status_text;
        this.txn_id = txn_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAmount1() {
        return amount1;
    }

    public void setAmount1(double amount1) {
        this.amount1 = amount1;
    }

    public double getAmount2() {
        return amount2;
    }

    public void setAmount2(double amount2) {
        this.amount2 = amount2;
    }

    public String getBuyer_name() {
        return buyer_name;
    }

    public void setBuyer_name(String buyer_name) {
        this.buyer_name = buyer_name;
    }

    public String getCurrency1() {
        return currency1;
    }

    public void setCurrency1(String currency1) {
        this.currency1 = currency1;
    }

    public String getCurrency2() {
        return currency2;
    }

    public void setCurrency2(String currency2) {
        this.currency2 = currency2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getIpn_id() {
        return ipn_id;
    }

    public void setIpn_id(String ipn_id) {
        this.ipn_id = ipn_id;
    }

    public String getIpn_mode() {
        return ipn_mode;
    }

    public void setIpn_mode(String ipn_mode) {
        this.ipn_mode = ipn_mode;
    }

    public String getIpn_type() {
        return ipn_type;
    }

    public void setIpn_type(String ipn_type) {
        this.ipn_type = ipn_type;
    }

    public float getIpn_version() {
        return ipn_version;
    }

    public void setIpn_version(float ipn_version) {
        this.ipn_version = ipn_version;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public double getReceived_amount() {
        return received_amount;
    }

    public void setReceived_amount(double received_amount) {
        this.received_amount = received_amount;
    }

    public double getReceived_confirms() {
        return received_confirms;
    }

    public void setReceived_confirms(double received_confirms) {
        this.received_confirms = received_confirms;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }

    public String getTxn_id() {
        return txn_id;
    }

    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }
}
