package com.vinbytes.binary.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NotificationDateFilter {

    private Date Date;
    private List<NotificationProperties> notificationProperties;
}
