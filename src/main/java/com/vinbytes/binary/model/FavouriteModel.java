package com.vinbytes.binary.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@ApiModel(description = "User Favourite Stocks")
@Document(collection = "Favourite")
public class FavouriteModel {

    @Id
    @ApiModelProperty(value = "Favourite id", required = true)
    private String id;

    @ApiModelProperty(value = "Favourite User id", required = true)
    private String userId;

    @ApiModelProperty(value = "Stocks", required = true)
    private List<FavouriteStocksModel> favouriteStocks;

    public FavouriteModel() {
    }

    public FavouriteModel(String id, String userId, List<FavouriteStocksModel> favouriteStocks) {
        this.id = id;
        this.userId = userId;
        this.favouriteStocks = favouriteStocks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<FavouriteStocksModel> getFavouriteStocks() {
        return favouriteStocks;
    }

    public void setFavouriteStocks(List<FavouriteStocksModel> favouriteStocks) {
        this.favouriteStocks = favouriteStocks;
    }
}
