package com.vinbytes.binary.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Document(collection = "purchaseBids")
public class PurchaseBidsModel {
    @Id
    private String id;
    private String stockName;
    private String stockValue;
    private String changeInStockValue;
    private String level;
    private String valueOfOneBid;
    private String noOfBids;
    private String transactionId;
    private Double totalAmount;
    private String transactionFee;
    private String biddingAmount;
    private String userId;
    private LocalDateTime biddingDate;
    private String stockId;
    private String changesBtw;
    private String status;
    private String symbol;
    private Double creditedUnitBtc;
    private String purchaseStatus;
    private String coinBaseUrl;
    private String coinBasePurchaseId;
    private PlayBidModel stockDetails;
    private Double exchangeRate1BTC = 0.00;
    private String exchangeRateString;


    public PurchaseBidsModel(){

    }

    public PurchaseBidsModel(String id, String stockName, String stockValue, String changeInStockValue, String level, String valueOfOneBid, String noOfBids, String transactionId, Double totalAmount, String transactionFee, String biddingAmount, String userId, LocalDateTime biddingDate, String stockId, String changesBtw, String status, String symbol, Double creditedUnitBtc, String purchaseStatus, String coinBaseUrl, String coinBasePurchaseId, PlayBidModel stockDetails, Double exchangeRate1BTC, String exchangeRateString) {
        this.id = id;
        this.stockName = stockName;
        this.stockValue = stockValue;
        this.changeInStockValue = changeInStockValue;
        this.level = level;
        this.valueOfOneBid = valueOfOneBid;
        this.noOfBids = noOfBids;
        this.transactionId = transactionId;
        this.totalAmount = totalAmount;
        this.transactionFee = transactionFee;
        this.biddingAmount = biddingAmount;
        this.userId = userId;
        this.biddingDate = biddingDate;
        this.stockId = stockId;
        this.changesBtw = changesBtw;
        this.status = status;
        this.symbol = symbol;
        this.creditedUnitBtc = creditedUnitBtc;
        this.purchaseStatus = purchaseStatus;
        this.coinBaseUrl = coinBaseUrl;
        this.coinBasePurchaseId = coinBasePurchaseId;
        this.stockDetails = stockDetails;
        this.exchangeRate1BTC = exchangeRate1BTC;
        this.exchangeRateString = exchangeRateString;
    }

    public Double getExchangeRate1BTC() {
        return exchangeRate1BTC;
    }

    public void setExchangeRate1BTC(Double exchangeRate1BTC) {
        this.exchangeRate1BTC = exchangeRate1BTC;
    }

    public PlayBidModel getStockDetails() {
        return stockDetails;
    }

    public void setStockDetails(PlayBidModel stockDetails) {
        this.stockDetails = stockDetails;
    }

    public String getCoinBaseUrl() {
        return coinBaseUrl;
    }

    public void setCoinBaseUrl(String coinBaseUrl) {
        this.coinBaseUrl = coinBaseUrl;
    }

    public String getPurchaseStatus() {
        return purchaseStatus;
    }

    public void setPurchaseStatus(String purchaseStatus) {
        this.purchaseStatus = purchaseStatus;
    }

    public Double getCreditedUnitBtc() {
        return creditedUnitBtc;
    }

    public void setCreditedUnitBtc(Double creditedUnitBtc) {
        this.creditedUnitBtc = creditedUnitBtc;
    }

    public String getChangesBtw() {
        return changesBtw;
    }

    public void setChangesBtw(String changesBtw) {
        this.changesBtw = changesBtw;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getTransactionFee() {
        return transactionFee;
    }

    public String getCoinBasePurchaseId() {
        return coinBasePurchaseId;
    }

    public void setCoinBasePurchaseId(String coinBasePurchaseId) {
        this.coinBasePurchaseId = coinBasePurchaseId;
    }

    public void setTransactionFee(String transactionFee) {
        this.transactionFee = transactionFee;
    }

    public String getBiddingAmount() {
        return biddingAmount;
    }

    public void setBiddingAmount(String biddingAmount) {
        this.biddingAmount = biddingAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockValue() {
        return stockValue;
    }

    public void setStockValue(String stockValue) {
        this.stockValue = stockValue;
    }

    public String getChangeInStockValue() {
        return changeInStockValue;
    }

    public void setChangeInStockValue(String changeInStockValue) {
        this.changeInStockValue = changeInStockValue;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getValueOfOneBid() {
        return valueOfOneBid;
    }

    public void setValueOfOneBid(String valueOfOneBid) {
        this.valueOfOneBid = valueOfOneBid;
    }

    public String getNoOfBids() {
        return noOfBids;
    }

    public void setNoOfBids(String noOfBids) {
        this.noOfBids = noOfBids;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public LocalDateTime getBiddingDate() {
        return biddingDate;
    }

    public void setBiddingDate(LocalDateTime biddingDate) {
        this.biddingDate = biddingDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getExchangeRateString() {
        return exchangeRateString;
    }

    public void setExchangeRateString(String exchangeRateString) {
        this.exchangeRateString = exchangeRateString;
    }

    @Override
    public String toString() {
        return "PurchaseBidsModel{" +
                "id='" + id + '\'' +
                ", stockName='" + stockName + '\'' +
                ", stockValue='" + stockValue + '\'' +
                ", changeInStockValue='" + changeInStockValue + '\'' +
                ", level='" + level + '\'' +
                ", valueOfOneBid='" + valueOfOneBid + '\'' +
                ", noOfBids='" + noOfBids + '\'' +
                ", totalAmount='" + totalAmount + '\'' +
                ", transactionFee='" + transactionFee + '\'' +
                ", biddingAmount='" + biddingAmount + '\'' +
                ", userId='" + userId + '\'' +
                ", biddingDate=" + biddingDate +
                ", stockId='" + stockId + '\'' +
                ", changesBtw='" + changesBtw + '\'' +
                '}';
    }
}
