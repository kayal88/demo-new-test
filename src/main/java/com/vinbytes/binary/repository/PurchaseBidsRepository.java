package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.PurchaseBidsModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseBidsRepository extends MongoRepository<PurchaseBidsModel, String> {

    boolean existsByStockName(String string);

    PurchaseBidsModel findByStockIdAndUserId(String stockId, String userId);

    List<PurchaseBidsModel> findAllByStockIdAndUserId(String stockId, String userId);

    List<PurchaseBidsModel> findAllByStockIdAndLevelAndChangesBtwAndPurchaseStatus(String stockId, String level, String changesBtw,String purchaseStatus);

   PurchaseBidsModel findByUserId(String userId);

    PurchaseBidsModel findByCoinBasePurchaseId(String coinBasePurchaseId);

    List<PurchaseBidsModel> findAllByUserId(String userId);

    List<PurchaseBidsModel> findAllByUserIdAndStatus(String stockId,String status);

    PurchaseBidsModel findByStockId(String stockId);

    PurchaseBidsModel findByTransactionId(String transactionId);


    Page<PurchaseBidsModel> findAllByUserId(String userId,Pageable pageable);

    Page<PurchaseBidsModel> findAllByUserIdAndStatus(String userId, String status, Pageable pageable);

}
