package com.vinbytes.binary.repository;

import com.vinbytes.binary.model.OverallProgressModal;
import org.springframework.data.mongodb.repository.MongoRepository;
/*Jul 09*/
public interface OverallProgressRepository extends MongoRepository<OverallProgressModal, String> {
}
