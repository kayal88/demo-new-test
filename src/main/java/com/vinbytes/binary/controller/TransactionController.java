package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.TransactionAddressRepository;
import com.vinbytes.binary.repository.TransactionRepository;
import com.vinbytes.binary.repository.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "Transaction Controller")
@CrossOrigin("*")
@RestController
@RequestMapping(value="/transaction")
public class TransactionController {

    @Autowired
    private TransactionAddressRepository transactionAddressRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private UserRepository userRepository;

    private static final Logger log = LoggerFactory.getLogger(TransactionController.class);

    @ApiOperation(value = "Transactions", notes = "POST API to update Transactions.")
    @PostMapping(value = "/update/{transactionId}")
    public ResponseEntity<?> UpdateAddress(@PathVariable("transactionId") String transactionId, @RequestBody TransactionAddress transactionAddress) {
        try {
            log.info("POST API to update Transactions.");
            Response res=new Response();
            TransactionModal transactionModal = transactionRepository.findByTransactionId(transactionId);
            if (transactionModal == null){
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Id is Incorrect"));
            }else {
                String userId = transactionModal.getUserId();
                UserTransactionAddress userTransaction = transactionAddressRepository.findByUserId(userId);
                if (userTransaction == null) {
                    UserTransactionAddress newUserTransaction = new UserTransactionAddress();
                    newUserTransaction.setUserId(userId);
                    List<TransactionAddress> userTransactionDetails = new ArrayList<>();
                    userTransactionDetails.add(transactionAddress);
                    newUserTransaction.setTransactionAddress(userTransactionDetails);
                    transactionAddressRepository.save(newUserTransaction);
                }else {
                    List<TransactionAddress> userTransactionDetails = userTransaction.getTransactionAddress();
                    userTransactionDetails.add(transactionAddress);
                    userTransaction.setTransactionAddress(userTransactionDetails);
                    transactionAddressRepository.save(userTransaction);
                }
                transactionModal.setTransactionAddress(transactionAddress.getAddress());
                transactionModal.setTransactionStatus("Transfer Amount");
                transactionRepository.save(transactionModal);
                res.setStatus("success");
                res.setResponseMessage("Transaction Address updated Successfully");
                return new ResponseEntity<Response>(res, HttpStatus.OK);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }

    @ApiOperation(value = "Transactions", notes = "GET API to find Previous Transactions.")
    @GetMapping(value = "/find/last/{userId}")
    public ResponseEntity<?> findIfAddressAlreadyAvailable(@PathVariable("userId") String userId) {
        try {
            log.info("GET API to find Previous Transactions.");
            UserTransactionAddress userTransaction = transactionAddressRepository.findByUserId(userId);
            List<TransactionAddress> userTransactionDetails = userTransaction.getTransactionAddress();
            return ResponseEntity.ok(userTransactionDetails.get(userTransactionDetails.size()-1));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }

    @ApiOperation(value = "Transactions", notes = "POST API to get User Transactions Details.")
    @GetMapping(value = "/get/{transactionId}")
    public ResponseEntity<?> GetTransactionDetailsOfUser(@PathVariable("transactionId") String transactionId) {
        try {
            log.info("POST API to get User Transactions Details.");
            TransactionModal transactionModal = transactionRepository.findByTransactionId(transactionId);
            if (transactionModal == null){
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Id is Incorrect"));
            }else {
                return ResponseEntity.ok(transactionModal);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }

    @ApiOperation(value = "Transactions", notes = "POST API to Update User Transactions Details.")
    @GetMapping(value = "/update/transaction/{transactionId}")
    public ResponseEntity<?> UpdateTransactionDetailsOfUser(@PathVariable("transactionId") String transactionId) {
        try {
            log.info("POST API to Update User Transactions Details.");
            TransactionModal transactionModal = transactionRepository.findByTransactionId(transactionId);
            if (transactionModal == null){
                return ResponseEntity.badRequest().body(new MessageResponse("Transaction Id is Incorrect"));
            }else {
                if (transactionModal.getTransactionType().equals("Winning Amount")){
                    transactionModal.setTransactionStatus("Transferred");
                }else {
                    transactionModal.setTransactionStatus("Refunded");
                }
                transactionRepository.save(transactionModal);
                return ResponseEntity.ok(transactionModal);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e));
        }
    }

    @ApiOperation(value = "Transactions", notes = "GET API to get User Top 3 Highest Transactions Details.")
    @GetMapping(value = "/get/highest/bidders")
    public ResponseEntity<?> GetTop3HighestBidders() {
        try {
            log.info("POST API to get User Transactions Details.");
            List<TransactionModal> transactionModal = transactionRepository.findTop3ByTransactionTypeOrderByBidAmountDesc("Bid Purchased");
            List<LiveBidders> LiveBidders1=new ArrayList<>();
            if (transactionModal == null){
                return ResponseEntity.badRequest().body(null);
            }else {
                for (TransactionModal singleTransactionModal : transactionModal){
                    String id = singleTransactionModal.getUserId();
                    UserModel userModel = userRepository.findById(id).orElse(null);
                    if (userModel != null){
                        LiveBidders liveBidders = new LiveBidders(userModel.getUsername(), userModel.getProfileUrl(),singleTransactionModal.getBidAmount());
                        LiveBidders1.add(liveBidders);
                    }
                }
                return ResponseEntity.ok(LiveBidders1);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e.getMessage()));
        }
    }


    @ApiOperation(value = "Transactions", notes = "GET API to get User Top 3 Highest Transactions Details.")
    @GetMapping(value = "/get/highest/bidders/won")
    public ResponseEntity<?> GetTop3HighestWonBidders() {
        try {
            log.info("POST API to get User Transactions Details.");
            List<TransactionModal> transactionModal = transactionRepository.findTop3ByTransactionTypeOrderByBidAmountDesc("Winning Amount");
            List<LiveBidders> LiveBidders1=new ArrayList<>();
            if (transactionModal == null){
                return ResponseEntity.badRequest().body(null);
            }else {
                for (TransactionModal singleTransactionModal : transactionModal){
                    String id = singleTransactionModal.getUserId();
                    UserModel userModel = userRepository.findById(id).orElse(null);
                    if (userModel != null){
                        LiveBidders liveBidders = new LiveBidders(userModel.getUsername(), userModel.getProfileUrl());
                        LiveBidders1.add(liveBidders);
                    }
                }
                return ResponseEntity.ok(LiveBidders1);
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error info " + e.getMessage()));
        }
    }

}
