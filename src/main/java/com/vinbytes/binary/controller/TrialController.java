package com.vinbytes.binary.controller;

import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.*;
import com.vinbytes.binary.services.GreetingServices;
import com.vinbytes.binary.services.Scheduler;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Controller
public class TrialController {

    private final GreetingServices greetingService;

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private NotifyMeRepository notifyMeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WalletRepository walletRepository;


    @Autowired
    private PlayBidRepository playBidRepository;

    @Autowired
    private DynamicSlabRepository dynamicSlabRepository;


    @Autowired
    private PurchaseBidsRepository purchaseBidsRepository;

    private final SimpMessagingTemplate simpMessagingTemplate;

    String WS4REDIS_HEARTBEAT = "--vbbeat--";

    TrialController(GreetingServices greetingService, Scheduler scheduler, NotifyMeRepository notifyMeRepository, SimpMessagingTemplate simpMessagingTemplate) {
        this.greetingService = greetingService;
        this.scheduler = scheduler;
        this.notifyMeRepository = notifyMeRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }



    @MessageMapping("/private-messages/{driverId}")
    @SendTo("/topic/private-messages/{driverId}")
    public void  greeting(@DestinationVariable String driverId, final Principal principal) throws InterruptedException {
        UserModel frmDb = userRepository.findByEmail(driverId);
        if(frmDb!=null)
        {
            frmDb.setRefId(principal.getName());
            userRepository.save(frmDb);
            greetingService.addUserName(principal.getName());
        }

    }


    @MessageMapping("/public/{bidId}")
    @SendTo("/topic/public/liveBidders")
    public void  LiveBiddersList(@DestinationVariable String bidId) throws InterruptedException {

        PlayBidModel model = playBidRepository.findByBidId(bidId);
        if (model != null) {
            simpMessagingTemplate.convertAndSend("/topic/public/liveBidders",model);
        }
    }


    @MessageMapping("/public/check")
    @SendTo("/topic/public/check")
    public void  pingPong()  {
            simpMessagingTemplate.convertAndSend("/topic/public/check",WS4REDIS_HEARTBEAT);
    }


    Double getCurrentCoinbaseValue(String urlString){
        try {
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            Reader streamReader = null;
            boolean Error = false;
            int status = con.getResponseCode();
            if (status > 299) {
                streamReader = new InputStreamReader(con.getErrorStream());
                Error = true;
            } else {
                streamReader = new InputStreamReader(con.getInputStream());
            }
            BufferedReader in = new BufferedReader(streamReader);
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            double CurrentPrice = 0.00;
            if (!Error){
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(String.valueOf(content));
                JSONObject data = (JSONObject) json.get("data");
                JSONObject prices = (JSONObject) data.get("prices");
                CurrentPrice = Double.parseDouble((String) prices.get("latest"));
            }
            return CurrentPrice;
        }catch (IOException | ParseException e){
            return 0.00;
        }
    }

}
