package com.vinbytes.binary.controller;

import com.vinbytes.binary.annotation.RequiresCaptcha;
import com.vinbytes.binary.message.request.LoginRequest;
import com.vinbytes.binary.model.FavouriteModel;
import com.vinbytes.binary.model.FavouriteStocksModel;
import com.vinbytes.binary.model.PlayBidModel;
import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.FavouriteRepository;
import com.vinbytes.binary.repository.PlayBidRepository;
import com.vinbytes.binary.repository.UserRepository;
import com.vinbytes.binary.services.KeyCloakService;
import com.vinbytes.binary.services.UserDetailsImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "favourite Controller")
@CrossOrigin("*")
@RestController
/*JUN 01*/
@RequestMapping(value="/favourite")
public class FavouriteController {

    private static final Logger log = LoggerFactory.getLogger(FavouriteController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FavouriteRepository favouriteRepository;

    @Autowired
    private PlayBidRepository playBidRepository;

    @Autowired
    private KeyCloakService keyCloakService;

    @RequestMapping(value = "/**", method = RequestMethod.OPTIONS)
    public void corsHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with");
        response.addHeader("Access-Control-Max-Age", "3600");
    }

    @ApiOperation(value = "Favourite", notes = "POST API to Add and Remove Favourite.")
    @PostMapping("/add/remove")
    public ResponseEntity<Object> FavouriteAddAndRemove(@ApiParam(value = "Favourite", required = true) @Valid @RequestBody FavouriteStocksModel favouriteStocks){
        log.info("inside FavouriteController.FavouriteAddAndRemove() method");
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userName = principal.toString();
            String userEmail = keyCloakService.getUserWithName(userName);
            if (!userEmail.equals("User Not Found !")) {
                UserModel user = userRepository.findByEmail(userEmail);
                if (user == null) {
                    return new ResponseEntity<>("User id " + userName + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    FavouriteModel favourite = favouriteRepository.findByUserId(user.getId());
                    if (favourite==null){
                        FavouriteModel favouriteAdd = new FavouriteModel();
                        favouriteAdd.setUserId(user.getId());
                        List<FavouriteStocksModel> favouriteStocksAdd = new ArrayList<>();
                        favouriteStocksAdd.add(favouriteStocks);
                        favouriteAdd.setFavouriteStocks(favouriteStocksAdd);
                        favouriteRepository.save(favouriteAdd);
                    }else {
                        List<FavouriteStocksModel> favouriteStocksFromDB = favourite.getFavouriteStocks();
                        boolean alreadyFavourite = false;
                        int index = 0;
                        for (FavouriteStocksModel fav : favouriteStocksFromDB){
                            if (fav.getStockName().equals(favouriteStocks.getStockName()) && fav.getSymbol().equals(favouriteStocks.getSymbol())){
                                alreadyFavourite = true;
                                index = favouriteStocksFromDB.indexOf(fav);
                            }
                        }
                        if(alreadyFavourite){
                            favouriteStocksFromDB.remove(index);
                        }else {
                            favouriteStocksFromDB.add(favouriteStocks);
                        }
                        favourite.setFavouriteStocks(favouriteStocksFromDB);
                        favouriteRepository.save(favourite);
                    }
                    FavouriteModel favouriteFromDB = favouriteRepository.findByUserId(user.getId());
                    return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Favourite Successfully Modified")
                            .body(favouriteFromDB);
                }
            }else {
                return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ApiOperation(value = "Favourites", notes = "GET API to get Favourite.")
    @GetMapping("/get")
    public ResponseEntity<Object> GetFavourites(){
        log.info("inside FavouriteController.GetFavourites() method");
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userName = principal.toString();
            String userEmail = keyCloakService.getUserWithName(userName);
            if (!userEmail.equals("User Not Found !")) {
                UserModel user = userRepository.findByEmail(userEmail);
                if (user == null) {
                    return new ResponseEntity<>("User id " + userName + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    FavouriteModel favourite = favouriteRepository.findByUserId(user.getId());
                    if (favourite==null){
                        return new ResponseEntity<>("Favourite Not Found", HttpStatus.NOT_FOUND);
                    }else {
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Favourite Successful")
                                .body(favourite);
                    }
                }
            }else {
                return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Favourites", notes = "GET API to get Favourite.")
    @GetMapping("/get/user/{userId}")
    public ResponseEntity<Object> GetFavouritesUsers(@PathVariable("userId") String userId){
        log.info("inside FavouriteController.GetFavourites() method");
        try {
            UserModel user = userRepository.findById(userId).orElse(null);



                if (user == null) {
                    return new ResponseEntity<>("User id " + userId + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    FavouriteModel favourite = favouriteRepository.findByUserId(user.getId());
                    if (favourite==null){
                        return new ResponseEntity<>("Favourite Not Found", HttpStatus.NOT_FOUND);
                    }else {
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Favourite Successful")
                                .body(favourite);
                    }

            }

        }catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Favourites", notes = "GET API to get progress Bids In Favourite.")
    @GetMapping("/get/progress/{response}/{symbol}")
    public ResponseEntity<Object> GetProgressBidsInFavourite(
            @ApiParam(value = "What To Send Back As Response", required = true) @PathVariable("response") String response,
            @ApiParam(value = "Symbol To get progress", required = true) @PathVariable("symbol") String symbol
    ){
        log.info("inside FavouriteController.GetProgressBidsInFavourite() method");
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userName = principal.toString();
            String userEmail = keyCloakService.getUserWithName(userName);
            if (!userEmail.equals("User Not Found !")) {
                UserModel user = userRepository.findByEmail(userEmail);
                if (user == null) {
                    return new ResponseEntity<>("User id " + userName + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    List<PlayBidModel> playBids = playBidRepository.findBySymbolAndBiddingStatus(symbol, "Open Now");
                    if (response.equals("count")){
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Favourite Count Successful")
                                .body(playBids.size());
                    }else if (response.equals("data")){
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Favourite Data Successful")
                                .body(playBids);
                    }else {
                        return new ResponseEntity<>("Bad Request !", HttpStatus.BAD_REQUEST);
                    }
                }
            }else {
                return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
            }

        }catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
