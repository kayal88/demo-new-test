package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.TransactionRepository;
import com.vinbytes.binary.repository.UserRepository;
import com.vinbytes.binary.repository.WalletRepository;
import com.vinbytes.binary.repository.coinPaymentsTransactionStatusRepository;
import com.vinbytes.binary.services.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Api(tags = "Wallet Controller")
@CrossOrigin("*")
@RestController
/*JUN 04*/
@RequestMapping(value="/wallet")
public class WalletController extends TransactionServices {

    private static final Logger log = LoggerFactory.getLogger(WalletController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private KeyCloakService keyCloakService;

    @Autowired
    private walletServices walletServices;


    @Autowired
    private NotificationService notificationService;

    @Autowired
    private coinPaymentsTransactionStatusRepository coinPaymentsTransactionStatusRepository;

    @RequestMapping(value = "/**", method = RequestMethod.OPTIONS)
    public void corsHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, x-requested-with");
        response.addHeader("Access-Control-Max-Age", "3600");
    }

    @ApiOperation(value = "Wallet", notes = "GET API to get User Wallet.")
    @GetMapping("/get")
    public ResponseEntity<Object> GetWallet(){
        log.info("inside WalletController.GetWallet() method");
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userName = principal.toString();
            String userEmail = keyCloakService.getUserWithName(userName);
            if (!userEmail.equals("User Not Found !")) {
                UserModel user = userRepository.findByEmail(userEmail);
                if (user == null) {
                    return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    WalletModel userWallet = walletRepository.findByUserId(user.getId());
                    if (userWallet==null){
                        return new ResponseEntity<>("Wallet Not Found", HttpStatus.NOT_FOUND);
                    }else {
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Wallet Successful")
                                .body(userWallet);
                    }
                }
            }else {
                return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
            }

        } catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Wallet", notes = "POST API to add test money in User Wallet.")
    @GetMapping("/get/allUser")
    public ResponseEntity<?> getAllUser(@PageableDefault(page = 0, size = 10, sort = "joinedOnDate", direction = Sort.Direction.DESC) Pageable pageable){
        log.info("inside WalletController.getUserAll() method");
        Page<UserModel> model =userRepository.findAll(pageable);


        if (model == null){
            return  ResponseEntity.badRequest().body("Users not found");
        }else {

            return ResponseEntity.ok().header("users", "Get All users  Successfully")
                    .body(model);
        }


    }

    @ApiOperation(value = "Wallet", notes = "GET API to get User Wallet.")
    @GetMapping("/get/user/{id}")
    public ResponseEntity<Object> GetWallet(@PathVariable("id") String id){
        log.info("inside WalletController.GetWallet() method");
        try {

                UserModel user = userRepository.findById(id).orElse(null);
                if (user == null) {
                    return new ResponseEntity<>("User id " + user + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    WalletModel userWallet = walletRepository.findByUserId(id);
                    if (userWallet==null){
                        return new ResponseEntity<>("Wallet Not Found", HttpStatus.NOT_FOUND);
                    }else {
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Wallet Successful")
                                .body(userWallet);
                    }
                }
            }

         catch (Exception e){
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "transaction", notes = "GET API to get User Wallet Transactions.")
    @GetMapping("/get/transaction")
    public ResponseEntity<Object> GetTransactionOfUser(@PageableDefault(page = 0, size = 10, sort = "biddingDate", direction = Sort.Direction.DESC) Pageable pageable){
        log.info("inside WalletController.GetWallet() method");
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userName = principal.toString();
            String userEmail = keyCloakService.getUserWithName(userName);
            if (!userEmail.equals("User Not Found !")) {
                UserModel user = userRepository.findByEmail(userEmail);
                if (user == null) {
                    return new ResponseEntity<>("User" + userName + " Not Found !", HttpStatus.NOT_FOUND);
                }else {
                    Page<TransactionModal> transactionModal =  transactionRepository.findAllByUserIdOrderByTransactionInitiateDateAndTimeDesc(user.getId(),pageable);
                    if (transactionModal==null){
                        System.out.println(transactionModal);
                        return new ResponseEntity<>("No Transactions Yet !", HttpStatus.NOT_FOUND);
                    }else {
                        return ResponseEntity.ok().header("Custom_Header_Of_Find_User", "Get Transactions of User Successful")
                                .body(transactionModal);
                    }
                }
            }else {
                return new ResponseEntity<>("User Not Found, Invalid Access Token !", HttpStatus.NOT_FOUND);
            }

        } catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>("Unhandled Error Occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Wallet", notes = "POST API to add test money in User Wallet.")
    @GetMapping("/add/test/money/{userId}")
    public String AddTestMoneyInWallet(@PathVariable("userId") String userId){
        log.info("inside WalletController.AddTestMoneyInWallet() method");
        try{
            UserModel user = userRepository.findById(userId).orElse(null);
            if (user == null){
                return "Test Money not successfully added in wallet, User Found Issue !";
            }else {
                TransactionModal transactionModal = new TransactionModal();
                transactionModal.setTotalAmount(10000.00);
                transactionModal.setUserId(userId);
                transactionModal.setUserName(user.getUsername());
                transactionModal.setTransactionMode("Coin Payments");
                transactionModal.setTransactionType("Add Money");
                transactionModal.setTransactionStatus("Credited");
                boolean transactionStatus = MadeTransaction(transactionModal);
                if (transactionStatus){
                    WalletModel userWallet = new WalletModel();
                    userWallet.setUserId(userId);
                    userWallet.setDollars(10000.00);
                    walletRepository.save(userWallet);
                    return "Test Money successfully added in wallet";
                }else {
                    return "Test Money not successfully added in wallet, Transaction Log Issue !";
                }
            }
        }catch (Exception e){
            return "Test Money not successfully added in wallet";
        }
    }


    @ApiOperation(value = "Wallet", notes = "POST API to update user activity.")
    @PostMapping("/update/user/activity/{data}/{userId}")
    public ResponseEntity<?> updateUserActivity(@PathVariable("data") String data, @PathVariable("userId") String userId){
        if(data!=null && data!="")
        {
            UserModel user = userRepository.findById(userId).orElse(null);

            if (user == null){
                return  ResponseEntity.badRequest().body("Users not found");
            }else {
                if(data.equals("Suspicious"))
                {
                    if(user.getSuspiciousCount()==0)
                    {
                        user.setSuspiciousCount(1);
                    }
                  else
                {
                    user.setSuspiciousCount(user.getSuspiciousCount()+1);
                }
                }
                else if (data.equals("Block"))
                {
                    user.setBlocked(true);
                    if(user.getBlockedCount()==0)
                    {
                        user.setBlockedCount(1);
                    }
                    else
                    {
                        user.setBlockedCount(user.getBlockedCount()+1);
                    }
                }
                else if (data.equals("Unblock"))
                {
                    user.setBlocked(false);
                }

                userRepository.save(user);
                return ResponseEntity.ok().header("users", "activity updated  Successfully")
                        .body(user);

            }
        }
        else
        {
            return  ResponseEntity.badRequest().body("Parameters missing");
        }

    }

    @ApiOperation(value = "Wallet", notes = "get API to get  top 10 User ")
    @GetMapping("/get/TopTenUser")
    public ResponseEntity<?> getTop10User(@PageableDefault(page = 0, size = 10, sort = "totalProfit", direction = Sort.Direction.DESC) Pageable pageable){
        log.info("inside WalletController.getUserAll() method");

        List<WalletModel> model =walletRepository.findTop10ByOrderByTotalProfitDesc();
        System.out.println(model);
        if (model == null){
            return  ResponseEntity.badRequest().body("Users not found");
        }else {

            return ResponseEntity.ok().header("users", "Get All users  Successfully")
                    .body(model);
        }
    }



/*    @ApiOperation(value = "Wallet", notes = "get API to get  top 10 User ")
    @GetMapping("/get/Transaction")
    public ResponseEntity<?> getAllTransaction(@PageableDefault(page = 0, size = 10,sort = "transactionInitiateDateAndTime", direction = Sort.Direction.DESC) Pageable pageable){
        log.info("inside TransactionController.getAllTransaction() method");

        Page<TransactionModal> model =transactionRepository.findAll(pageable);
        System.out.println(model);
        if (model == null){
            return  ResponseEntity.badRequest().body("Users not found");
        }else {

            return ResponseEntity.ok().header("users", "Get All users  Successfully")
                    .body(model);
        }
    }*/

    @ApiOperation(value = "Get transaction log", notes = "Get transaction log.")
    @GetMapping(value = "get/transaction/{userId}")
    @ApiParam(value = "Get transactionlog", required = true)
    public ResponseEntity<?> getTransactionByStatus(@PathVariable("userId") String userId,
                                                    @PageableDefault(page = 0, size = 10, sort = "biddingDate", direction = Sort.Direction.DESC) Pageable pageable) {

        Response res=new Response();
        try {
            log.info("inside PlayBidController.getAllPlayBidDetails() method");
            Page<TransactionModal> model = transactionRepository.findAllByUserIdOrderByTransactionInitiateDateAndTimeDesc(userId,pageable);
            System.out.println(model);
            if(model==null)
            {
                return  ResponseEntity.badRequest().body("Users not found");
            }
            else
            {
                return ResponseEntity.ok().header("users", "Get All users  Successfully")
                        .body(model);
            }

        }
        catch (Exception e){
            res.setStatus("success");
            res.setResponseMessage("error "+e);
            return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @ApiOperation(value = "Get transaction log", notes = "Get transaction log.")
    @GetMapping(value = "/search/byStatus/{userId}/{status}")
    @ApiParam(value = "Get transaction Log", required = true)
    public ResponseEntity<Response> getTransactionByStatus(@PathVariable("userId") String userId,
            @PathVariable("status") String status,
            @PageableDefault(page = 0, size = 10,sort = "transactionInitiateDateAndTime", direction = Sort.Direction.DESC) Pageable pageable) {

        Response res=new Response();
        try {
            log.info("inside PlayBidController.getAllPlayBidDetails() method");
            Page<TransactionModal>  model;
            if(status.equals("All"))
            {

                 model=transactionRepository.findAllByUserId(userId,pageable);
            }
            else
            {
                model = transactionRepository.findAllByUserIdAndTransactionStatusOrderByTransactionInitiateDateAndTimeDesc(userId,status,pageable);
            }
            res.setPageData(model);
            return new ResponseEntity<Response>(res, HttpStatus.OK);
        }
        catch (Exception e){
            res.setStatus("success");
            res.setResponseMessage("error "+e);
            return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ApiOperation(value = "Wallet", notes = "POST API to Generate Url to money in User Wallet from another wallet using coinbase Gateway Interaction")
    @GetMapping("/add/money/{userId}/{money}")
    public ResponseEntity<Response> addMoneyInWallet(@PathVariable("money") String money, @PathVariable("userId") String userId){
        log.info("inside WalletController.addMoneyInWallet() method");
        Response res=new Response();
        try {
            UserModel user = userRepository.findById(userId).orElse(null);
            if (user == null){
                res.setStatus("failure");
                res.setResponseMessage("Url Not Generate Successfully, User Not Found !");
                return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
            }else {
                addMoneyRequestModel addMoneyRequestModel = new addMoneyRequestModel(userId, Double.parseDouble(money), "BTC");
                coinPaymentsDepositResponseModel coinPaymentsDepositResponseModel =  walletServices.walletDepositRequest(addMoneyRequestModel);

                if(coinPaymentsDepositResponseModel==null)
                {
                    res.setStatus("failure");
                    res.setResponseMessage("We're facing some issue");
                    return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
                }
                boolean walletStatus = walletServices.walletDepositInit(userId, coinPaymentsDepositResponseModel);
                if (walletStatus){
                    /*notificationService.MakeNotifications(user.getId(), "Your Deposit of "+money+" was initiated.", "GET!SET!GO!!!", user.getProfileUrl(), "/wallet");*/
                    res.setStatus("Success, Url Generate Successfully");
                    res.setResponseMessage(coinPaymentsDepositResponseModel.getCheckout_url());
                    return new ResponseEntity<Response>(res, HttpStatus.OK);
                }else {
                    res.setStatus("failure");
                    res.setResponseMessage("Url Not Generate Successfully, Transaction Issue !");
                    return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }catch (Exception e){
            res.setStatus("failure");
            res.setResponseMessage("Url Not Generate Successfully");
            return new ResponseEntity<Response>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/withdrawal")
    public ResponseEntity<?> WalletWithdrawalRequest(@RequestBody withdrawalModel withdrawalModel){

        try{
            log.info("Inside walletController.WalletWithdrawalRequest()");
            if (withdrawalModel.getAddress()==null || withdrawalModel.getAddress().equals("") || withdrawalModel.getAmount()<=0){
                return ResponseEntity.badRequest().body("Bad Request !");
            }else {
                UserModel userFromDB = userRepository.findById(withdrawalModel.getUserId()).orElse(null);
                System.out.println();
                if (userFromDB != null){
                    WalletModel walletModel = walletRepository.findById(userFromDB.getWalletId()).orElse(null);
                    if (walletModel == null){
                        return ResponseEntity.badRequest().body("Wallet Not Found !");
                    }else {
                        String userId = userFromDB.getId();
                        Double withdrawAmount = withdrawalModel.getAmount();

                        if (walletModel.getDollars() >= withdrawAmount){
                            coinPaymentsWithdrawalResponseModel coinPaymentsWithdrawalResponseModel =  walletServices.walletWithdrawalRequest(withdrawalModel);
                            boolean walletStatus = walletServices.walletWithdrawalInit(userId, coinPaymentsWithdrawalResponseModel, withdrawalModel);
                            if (walletStatus){
                                /*notificationService.MakeNotifications(userFromDB.getId(), "Your withdrawAmount of "+withdrawAmount+" was initiated.", "GET!SET!GO!!!", userFromDB.getProfileUrl(), "/wallet");*/
                                return ResponseEntity.ok(new MessageResponse("Withdrawal Request Successfully submitted !"));
                            }else {
                                return ResponseEntity.badRequest().body("failed to find your wallet !!");
                            }
                        }else {
                            return ResponseEntity.badRequest().body("Withdrawal failed, not having sufficient Balance in wallet !");
                        }
                    }
                }else{
                    return ResponseEntity.badRequest().body("Unauthorized User !!");
                }
            }
        }catch (Exception e){

            return ResponseEntity.badRequest().body(new MessageResponse(e.getMessage()));
        }
    }

    @PostMapping("/coin-payments/deposit")
    public void CoinPaymentsDeposit(IPNFRFModel ipnfrfModel, @RequestHeader("Hmac") String Hmac, @RequestBody String request){
        try {
            log.info("Inside walletController.CoinPaymentsDeposit()");
            boolean coinPaymentsSecurityCheck = walletServices.coinPaymentsSecurityCheck(ipnfrfModel.getMerchant(), Hmac, request);
            if (coinPaymentsSecurityCheck){
                coinPaymentsTransactionStatusResponseModel coinPaymentsTransactionStatusResponseModel = coinPaymentsTransactionStatusRepository.findByCoinPaymentsId(ipnfrfModel.getTxn_id());
                List<IPNFRFModel> ipnfrfModelList = new ArrayList<>();
                if (coinPaymentsTransactionStatusResponseModel == null){
                    coinPaymentsTransactionStatusResponseModel = new coinPaymentsTransactionStatusResponseModel();
                    coinPaymentsTransactionStatusResponseModel.setCoinPaymentsId(ipnfrfModel.getTxn_id());
                }else {
                    ipnfrfModelList = coinPaymentsTransactionStatusResponseModel.getIpnfrf();
                }
                ipnfrfModelList.add(ipnfrfModel);
                long status = ipnfrfModel.getStatus();
                coinPaymentsTransactionStatusResponseModel.setIpnfrf(ipnfrfModelList);
                coinPaymentsTransactionStatusResponseModel.setDateAndTime(new Date());
                coinPaymentsTransactionStatusResponseModel.setStatus(status);
                coinPaymentsTransactionStatusResponseModel.setStatusText(ipnfrfModel.getStatus_text());
                coinPaymentsTransactionStatusRepository.save(coinPaymentsTransactionStatusResponseModel);
                TransactionModal transactionModel = transactionRepository.findByCoinPaymentsDepositResponseTxnId(ipnfrfModel.getTxn_id());
                if (transactionModel == null){
                    log.error("Wrong Transaction Request ! Reach out Admin");
                }else {
                    Double receivedAmount = ipnfrfModel.getReceived_amount();
                    String transactionStatus = coinPaymentsTransactionStatusResponseModel.getStatusText();
                    transactionStatusModel transactionStatusModel = new transactionStatusModel(new Date(), transactionStatus, receivedAmount);
                    List<transactionStatusModel> transactionStatusModelList = transactionModel.getTransactionStatusList();
                    if (transactionStatusModelList.size()==0){
                        transactionStatusModelList = new ArrayList<>();
                    }
                    transactionStatusModelList.add(transactionStatusModel);
                    transactionModel.setTransactionStatus(transactionStatus);
                    transactionModel.setReceivedAmount(receivedAmount);
                    boolean walletTransactionState = transactionModel.isTransactionState();
                    boolean walletDepositState = false;
                    if (walletTransactionState){
                        if (status >= 100 || status == 2) {
                            // payment is complete or queued for nightly payout, success
                            walletTransactionState = walletServices.walletDepositPayment(transactionModel);
                            walletDepositState = walletTransactionState;

                            log.info("payment is completed");
                        } else if (status < 0) {
                            //payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent
                            walletTransactionState = walletServices.walletDepositPayment(transactionModel);
                            walletDepositState = walletTransactionState;
                            log.info("payment error");
                        } else {
                            //payment is pending, you can optionally add a note to the order page
                            log.info("payment is pending");
                        }
                    }
                    if(walletDepositState)
                    {
                        transactionModel.setTransactionState(false);
                    }
                    transactionRepository.save(transactionModel);
                }
            }else {
                log.error("CoinPayments Security Issue !!");
            }
        }catch (Exception e){
            log.error("Unhandled Exception Occurred : " + e.getMessage());
        }
    }


    @PostMapping("/coin-payments/withdrawal")
    public void CoinPaymentsWithdrawal(IPNFWFModel ipnfwfModel, @RequestHeader("Hmac") String Hmac, @RequestBody String request){
        try {
            log.info("Inside walletController.CoinPaymentsWithdrawal()");
            boolean coinPaymentsSecurityCheck = walletServices.coinPaymentsSecurityCheck(ipnfwfModel.getMerchant(), Hmac, request);
            if (coinPaymentsSecurityCheck){
                coinPaymentsTransactionStatusResponseModel coinPaymentsTransactionStatusResponseModel = coinPaymentsTransactionStatusRepository.findByCoinPaymentsId(ipnfwfModel.getId());
                List<IPNFWFModel> ipnfwfModelList = new ArrayList<>();
                if (coinPaymentsTransactionStatusResponseModel == null){
                    coinPaymentsTransactionStatusResponseModel = new coinPaymentsTransactionStatusResponseModel();
                    coinPaymentsTransactionStatusResponseModel.setCoinPaymentsId(ipnfwfModel.getId());
                }else {
                    ipnfwfModelList = coinPaymentsTransactionStatusResponseModel.getIpnfwf();
                }
                ipnfwfModelList.add(ipnfwfModel);
                long status = ipnfwfModel.getStatus();
                coinPaymentsTransactionStatusResponseModel.setIpnfwf(ipnfwfModelList);
                coinPaymentsTransactionStatusResponseModel.setDateAndTime(new Date());
                coinPaymentsTransactionStatusResponseModel.setStatus(status);
                coinPaymentsTransactionStatusResponseModel.setStatusText(ipnfwfModel.getStatus_text());
                coinPaymentsTransactionStatusRepository.save(coinPaymentsTransactionStatusResponseModel);
                TransactionModal transactionModel = transactionRepository.findByCoinPaymentsWithdrawalResponseId(ipnfwfModel.getId());
                if (transactionModel == null){
                    log.error("Wrong Transaction Request ! Reach out Admin");
                }else {
                    String transactionStatus = coinPaymentsTransactionStatusResponseModel.getStatusText();
                    transactionStatusModel transactionStatusModel = new transactionStatusModel(new Date(), transactionStatus, 0.00);
                    List<transactionStatusModel> transactionStatusModelList = transactionModel.getTransactionStatusList();
                    if (transactionStatusModelList.size()==0){
                        transactionStatusModelList = new ArrayList<>();
                    }
                    transactionStatusModelList.add(transactionStatusModel);
                    transactionModel.setTransactionStatus(transactionStatus);
                    transactionModel.setReceivedAmount(0.00);
                    boolean walletTransactionState = transactionModel.isTransactionState();
                    if (walletTransactionState){
                        if (status >= 100 || status == 2) {
                            // payment is complete or queued for nightly payout, success
                            walletTransactionState = walletServices.walletDepositPayment(transactionModel);
                            log.info("payment is completed");
                        } else if (status < 0) {
                            //payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent
                            walletTransactionState = walletServices.walletDepositPayment(transactionModel);
                            log.info("payment error");
                        } else {
                            //payment is pending, you can optionally add a note to the order page
                            log.info("payment is pending");
                        }
                    }
                    if (walletTransactionState){
                        transactionModel.setTransactionState(false);
                    }
                    transactionRepository.save(transactionModel);
                }
            }else {
                log.error("CoinPayments Security Issue !!");
            }
        }catch (Exception e){
            System.out.println("Unhandled Exception Occurred : " + e.getMessage());
        }
    }

    @GetMapping("/get/transactions/{userId}/{walletType}")
    public ResponseEntity<?> getWalletTransactions(@PathVariable String userId, @PathVariable String walletType,
                                                   @PageableDefault(page = 0, size = 10, sort = "biddingDate", direction = Sort.Direction.DESC) Pageable pageable){
        try{
            log.info("Inside walletController.getWalletTransactions()");
            Page<TransactionModal> transactionModelList;
//            List<TransactionModal> transactionModelList;
            if (walletType.equals("All")){

                transactionModelList = transactionRepository.findAllByUserIdOrderByTransactionInitiateDateAndTimeDesc(userId,pageable);
            }else {
//                walletType += " Wallet";
                transactionModelList = transactionRepository.findAllByUserIdAndWalletTypeOrderByTransactionInitiateDateAndTimeDesc(userId, walletType, pageable);

            }
            return ResponseEntity.ok(transactionModelList);
        }catch (Exception e){
            return ResponseEntity.badRequest().body("Unhandled Exception Occurred : " + e.getMessage());
        }
    }


    @ApiOperation(value = "Wallet", notes = "get API to get Top 5 LeaderBoard ")
    @GetMapping("/get/leaderboard")
    public ResponseEntity<?> getTop5LeaderBoard(){
        log.info("inside WalletController.getTop5LeaderBoard() method");

        List<WalletModel> model = walletRepository.findTop5ByOrderByTotalProfitDesc();
        if (model == null){
            return  ResponseEntity.badRequest().body(null);
        }else {
            for(WalletModel singleWalletModel : model){
                String id = singleWalletModel.getUserId();
                UserModel userModel = userRepository.findById(id).orElse(null);
                if (userModel != null){
                    singleWalletModel.setId(userModel.getUsername());
                    singleWalletModel.setUserId(userModel.getProfileUrl());
                }
            }
            return ResponseEntity.ok().header("users", "Get All top users  Successfully")
                    .body(model);
        }
    }


}
