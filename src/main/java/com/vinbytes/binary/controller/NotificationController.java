package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.NotificationDateFilter;
import com.vinbytes.binary.model.NotificationModel;
import com.vinbytes.binary.model.NotificationProperties;
import com.vinbytes.binary.model.UserModel;
import com.vinbytes.binary.repository.NotificationsRepository;
import com.vinbytes.binary.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)
@RestController
@RequestMapping(value="/notification")

public class NotificationController {

    @Autowired
    private NotificationsRepository notificationsRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping(value = "/read/{notificationId}")
    public ResponseEntity<?> NotificationRead(@PathVariable String notificationId){
        try
        {
            NotificationModel notificationModel = notificationsRepository.findById(notificationId).orElse(null);
            if (notificationModel==null){
                return new ResponseEntity<>("No Notification found", HttpStatus.NOT_FOUND);
            }else{
                List<NotificationDateFilter> notificationDateFilter = notificationModel.getNotificationDateFilters();
                List<NotificationProperties> notificationProperties = notificationDateFilter.get(0).getNotificationProperties();
                notificationProperties.get(0).setChecked(true);
                notificationDateFilter.get(0).setNotificationProperties(notificationProperties);
                notificationModel.setNotificationDateFilters(notificationDateFilter);
                notificationsRepository.save(notificationModel);
                return ResponseEntity.ok(new MessageResponse("Notification modify success"));
            }
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return new ResponseEntity<>("Unhandled error !", HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping(value = "/readAll/{userId}")
    public ResponseEntity<?> NotificationReadAll(@PathVariable String userId){
        try {
            List<NotificationModel> notificationModel = notificationsRepository.findByUserIdAndNotificationDateFiltersNotificationPropertiesChecked(userId,false);
            if (notificationModel == null) {
                return new ResponseEntity<>("No Notification found", HttpStatus.NOT_FOUND);
            } else {

            for (NotificationModel frmDb1 : notificationModel) {

                List<NotificationDateFilter> notificationDateFilter = frmDb1.getNotificationDateFilters();
                List<NotificationProperties> notificationProperties = notificationDateFilter.get(0).getNotificationProperties();
                notificationProperties.get(0).setChecked(true);
                notificationDateFilter.get(0).setNotificationProperties(notificationProperties);
                frmDb1.setNotificationDateFilters(notificationDateFilter);
                notificationsRepository.save(frmDb1);
            }
                return ResponseEntity.ok(new MessageResponse("Notification modify success"));
        }
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return new ResponseEntity<>("Unhandled error !", HttpStatus.NOT_FOUND);
        }
    }



    @GetMapping(value = "/get/{emailId}")
    public ResponseEntity<?> getNotification(@PathVariable String emailId){
        try {
            UserModel frmDb = userRepository.findByEmail(emailId);

            List<NotificationModel> notification = notificationsRepository.findTop15ByUserIdOrderByNotificationDateFiltersNotificationPropertiesDateAndTimeDesc(frmDb.getId());
            return ResponseEntity.ok().body(notification);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return ResponseEntity.ok(new MessageResponse("Notification modify success"));
        }
    }



}