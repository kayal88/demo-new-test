package com.vinbytes.binary.controller;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.ContactModel;
import com.vinbytes.binary.model.HelpDeskModel;
import com.vinbytes.binary.repository.ContactRepository;
import com.vinbytes.binary.repository.HelpDeskRepository;
import com.vinbytes.binary.services.EmailService;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)

@RestController
@RequestMapping("/support/")
public class contactController {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private HelpDeskRepository helpDeskRepository;

    @Autowired
    private EmailService emailService;

    @PostMapping("contact")
    public ResponseEntity<?> contact(@RequestBody ContactModel contactModel){
        try {
            contactRepository.save(contactModel);
            emailService.sendSupportMail(contactModel.getName(), contactModel.getEmail(), contactModel.getPhone(), contactModel.getDescription(), "", "contact");
            return ResponseEntity.ok("submitted successfully !");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: " + e.getMessage()));
        }
    }

    @PostMapping("helpdesk")
    public ResponseEntity<?> helpdesk(@RequestBody HelpDeskModel helpDeskModel){
        try {
            helpDeskRepository.save(helpDeskModel);
            emailService.sendSupportMail("", helpDeskModel.getEmail(), 0, helpDeskModel.getDescription(), helpDeskModel.getSubject(), "helpdesk");
            return ResponseEntity.ok("submitted successfully !");
        }catch (Exception e){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: " + e.getMessage()));
        }
    }
}
