package com.vinbytes.binary.configuration;

import com.vinbytes.binary.message.response.MessageResponse;
import com.vinbytes.binary.model.*;
import com.vinbytes.binary.repository.*;
import com.vinbytes.binary.services.*;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/stocks/")
public class AutomationConfig extends TransactionServices {

    @Autowired
    private PlayBidRepository playBidRepository;

    @Autowired
    private PurchaseBidsRepository purchaseBidsRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private OverallProgressRepository overallProgressRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CoinBaseTransactionRepository coinBaseTransactionRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private  NotificationService notificationService;

    @Autowired
    private CoinBasePaymentServices coinBasePaymentServices;

    @Autowired
    private DynamicSlabRepository dynamicSlabRepository;

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    EmailService emailService;

    @PostMapping("create/{min}")
    public void CronBidAutomationCreateStock(@PathVariable("min") int min){
        boolean bidCreation = createPlayBid(min);
        if (bidCreation){
            System.out.println( min+ "min Bid Created successfully by Automation");
        }else {
            System.out.println( min+ "min Bid Created not successfully by Automation");
        }
    }

    @PostMapping("open/{min}")
    public void CronBidAutomationOpenStock(@PathVariable("min") String min){
        boolean bidOpen = openNow(min);
        /*if (bidOpen){
            System.out.println("3min Bid Open successfully by Automation");
        }else {
            System.out.println("3min Bid Open not successfully by Automation");
        }*/
    }

    @PostMapping("close/{min}")
    public void CronBidAutomationCloseStock(@PathVariable("min") String min){
        boolean bidClose = stockUpdateByStartAndEndAndResultTime(min);
        /*if (bidClose){
            System.out.println("3min Bid Close successfully by Automation");
        }else {
            System.out.println("3min Bid Close not successfully by Automation");
        }*/
    }

    boolean openNow(String bidType){


        try{
            //        System.out.println(new Date());
            PlayBidModel biddingClose = playBidRepository.findTop1ByBiddingStatusAndBidTypeOrderByBiddingClosesAtAsc("Open Now", bidType);

//        PlayBidModel biddingClose = playBidRepository.findTop1ByBiddingStatusOrderByBiddingClosesAtAsc("Open Now");

            /*Jul 09*/
            List<OverallProgressModal> overallProgressModalList = overallProgressRepository.findAll();

            if(overallProgressModalList.size()>0){
                OverallProgressModal overallProgressModal = overallProgressModalList.get(0);
                if (biddingClose != null && new Date().compareTo(biddingClose.getBiddingClosesAt())>=0){
                    if (biddingClose.getUserId() !=null && biddingClose.getUserId().size()>0) {

                        List<Double> TotalNoOfAmountInSlab = new ArrayList<>(biddingClose.getTotalNoOfAmountInSlab());
                        List<Double> TotalNoOfAmountInSlabForDB = new ArrayList<>(biddingClose.getTotalNoOfAmountInSlab());
                        Double MaxNoOfAmountInSlab = 0.00;
                        Double SumOfAmountInSlab = 0.00;
                        if (TotalNoOfAmountInSlab.size() > 0) {
                            Collections.sort(TotalNoOfAmountInSlab);
                            MaxNoOfAmountInSlab = TotalNoOfAmountInSlab.get(TotalNoOfAmountInSlab.size() - 1);
                            SumOfAmountInSlab = TotalNoOfAmountInSlab.stream().reduce(0.00, Double::sum);
                            /*if (((SumOfAmountInSlab - MaxNoOfAmountInSlab) <= (MaxNoOfAmountInSlab * 0.1)) || biddingClose.getUserId() == null || biddingClose.getUserId().size() <= 1) {*/
                            if (((SumOfAmountInSlab - MaxNoOfAmountInSlab) <= (MaxNoOfAmountInSlab * 0.1)) || biddingClose.getUserId() == null || biddingClose.getUserId().size() <= 0) {
                                biddingClose.setBiddingStatus("Cancelled");
                                overallProgressModal.setTotalNoOfLockedBids(overallProgressModal.getTotalNoOfLockedBids() + 1);
                                for (String id : biddingClose.getUserId()) {
                                    UserModel user = userRepository.findById(id).orElse(null);

                                    if (user != null) {
                                        WalletModel userWallet = walletRepository.findById(user.getWalletId()).orElse(null);
                                        List<PurchaseBidsModel> allBidsPurchasedByUserInThisSameStock = purchaseBidsRepository.findAllByStockIdAndUserId(biddingClose.getBidId(), id);
                                        for (PurchaseBidsModel oneBidPurchasedByUserInThisSameStock : allBidsPurchasedByUserInThisSameStock) {
                                            oneBidPurchasedByUserInThisSameStock.setStatus("Refund Initiated");
                                            purchaseBidsRepository.save(oneBidPurchasedByUserInThisSameStock);
                                            TransactionModal transactionModal = new TransactionModal();
                                            Double AmountOfOneUser = oneBidPurchasedByUserInThisSameStock.getTotalAmount();
                                            transactionModal.setTotalAmount(AmountOfOneUser);
                                            Double CurrentBTCPrice = getCurrentCoinbaseValue("https://www.coinbase.com/api/v2/assets/prices/bitcoin?base=USD");
                                            transactionModal.setCreditedUnitBtc(AmountOfOneUser);
                                            oneBidPurchasedByUserInThisSameStock.setCreditedUnitBtc(AmountOfOneUser);
                                            transactionModal.setStockId(biddingClose.getBidId());
                                            transactionModal.setTransactionInitiateDateAndTime(new Date());
                                            transactionModal.setStockName(biddingClose.getStockName());
                                            transactionModal.setStockSymbol(biddingClose.getSymbol());
                                            transactionModal.setUserId(user.getId());
                                            transactionModal.setUserName(user.getUsername());
                                            transactionModal.setTransactionMode("Wallet");
                                            transactionModal.setTransactionType("Refund Amount");
                                            transactionModal.setTransactionStatus("Credited");
                                            boolean transactionStatus = MadeTransaction(transactionModal);
                                            if (transactionStatus && userWallet != null) {

                                                userWallet.setDollars(userWallet.getDollars() + AmountOfOneUser);
                                                userWallet.setTotalCredit(userWallet.getTotalCredit() + AmountOfOneUser);
                                                userWallet.setTotalNumberOfTimesCredit(userWallet.getTotalNumberOfTimesCredit() + 1);
                                                userWallet.setTotalNumberOfProfit(userWallet.getTotalNumberOfProfit() + 1);
                                                walletRepository.save(userWallet);
                                            } else {
                                                System.out.println("There is a Transaction Issue ! UserId == " + id);
                                            }
                                /*if (!transactionStatus){
                                    System.out.println("There is a Transaction Issue ! UserId == "+id+" !");
                                }*/
                                /*List<TransactionModal> allTransactionModal = transactionRepository.findAllByUserIdAndTransactionId(id,oneBidPurchasedByUserInThisSameStock.getTransactionId());
                                for (TransactionModal oneTransaction : allTransactionModal){
                                    oneTransaction.setTransactionStatus("Awaiting Response");
                                    transactionRepository.save(oneTransaction);
                                }*/
                                        }
                                    }
                                }
                            } else {
                                biddingClose.setBiddingStatus("Closed");
                                String coin = biddingClose.getStockName().toLowerCase().split(" ")[0];
                                if (coin.equals("binance")) {
                                    coin = "eb8ab5cb-d2a5-5068-88af-21c3ed92757a";
                                }
                                overallProgressModal.setTotalNoOfClosedBids(overallProgressModal.getTotalNoOfClosedBids() + 1);
                            }
                        } else {
                            biddingClose.setBiddingStatus("Cancelled");
                            overallProgressModal.setTotalNoOfLockedBids(overallProgressModal.getTotalNoOfLockedBids() + 1);
                        }
                        biddingClose.setTotalNoOfAmountInSlab(TotalNoOfAmountInSlabForDB);
                    }
                    else {
                        biddingClose.setBiddingStatus("Cancelled");
                        overallProgressModal.setTotalNoOfLockedBids(overallProgressModal.getTotalNoOfLockedBids() + 1);
                    }
                    overallProgressModal.setTotalNoOfInProgressBids(overallProgressModal.getTotalNoOfInProgressBids()-1);
                    biddingClose.setBiddingClosedAtTime(new Date());
                    playBidRepository.save(biddingClose);

                }
                overallProgressRepository.save(overallProgressModal);
            }
            return true;
        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }




    boolean stockUpdateByStartAndEndAndResultTime(String bidType){


        try {
            PlayBidModel biddingDraw = playBidRepository.findTop1ByBiddingStatusAndBidTypeOrderByDrawTimeAsc("Closed", bidType);

            /*Jul 09*/
            List<OverallProgressModal> overallProgressModalList = overallProgressRepository.findAll();

            if(overallProgressModalList.size()>0){
                OverallProgressModal overallProgressModal = overallProgressModalList.get(0);
                if (biddingDraw != null && new Date().compareTo(biddingDraw.getDrawTime())>=0){
                    biddingDraw.setBiddingStatus("Completed");
                    String coin = biddingDraw.getStockName().toLowerCase().split(" ")[0];
                    if (coin.equals("binance")){
                        coin = "eb8ab5cb-d2a5-5068-88af-21c3ed92757a";
                    }
                    Double CurrentPrice = getCurrentCoinbaseValue("https://www.coinbase.com/api/v2/assets/prices/"+coin+"?base=USD");
                    if (CurrentPrice < 0){
                        System.out.println("error");
                    }else {
                        biddingDraw.setDrawTimeResults(CurrentPrice);
                        String level;
                        double differenceBetweenDrawTimeResultsAndCloseTimeResultsUpOrDown = biddingDraw.getDrawTimeResults() - biddingDraw.getClosedTimeResults();
                        if (differenceBetweenDrawTimeResultsAndCloseTimeResultsUpOrDown < 0){
                            level = "Down";
                        }else {
                            level = "Up";
                        }
                        if (biddingDraw.getUserId()==null){
                            System.out.println("No one purchase This Stock Yet !");
                        }else {
                            Double totalAmount;
                            double differenceBetweenDrawTimeResultsAndCloseTimeResults = Math.round(Math.abs(biddingDraw.getDrawTimeResults() - biddingDraw.getClosedTimeResults()));
                            double totalNoOfBidsUsersWon = 0.00;
                            biddingDraw.setWinningLevel(level);

                            DynamicSlabModel dynamicSlabModel = dynamicSlabRepository.findByType(biddingDraw.getBidType());
                            if (dynamicSlabModel == null) {
                                System.out.println("Dynamic Slab Not Found !");
                            } else {
                                List<DynamicSlabValues> dynamicSlabValuesList = dynamicSlabModel.getDynamicSlabValues();
                                String winningSlab = null;
                                for (DynamicSlabValues dynamicSlabValues : dynamicSlabValuesList){
                                    long fromValue = Math.round(dynamicSlabValues.getFromValue());
                                    long toValue = Math.round(dynamicSlabValues.getToValue());
                                    if (fromValue == toValue){
                                        if (differenceBetweenDrawTimeResultsAndCloseTimeResults > fromValue){
                                            winningSlab = "over $" + fromValue;
                                        }
                                    }else {

                                        if (differenceBetweenDrawTimeResultsAndCloseTimeResults >= fromValue && differenceBetweenDrawTimeResultsAndCloseTimeResults <= toValue){
                                            winningSlab = "$" + fromValue + " - $" + toValue;
                                        }
                                    }
                                }
                                biddingDraw.setWinningSlab(winningSlab);
                                for (String id : biddingDraw.getUserId()) {
                                    List<PurchaseBidsModel> allBidsPurchasedByUserInThisSameStock = purchaseBidsRepository.findAllByStockIdAndUserId(biddingDraw.getBidId(), id);
                                    for (PurchaseBidsModel oneBidPurchasedByUserInThisSameStock : allBidsPurchasedByUserInThisSameStock) {
                                        if (level.equals(oneBidPurchasedByUserInThisSameStock.getLevel())) {
                                            if (winningSlab ==null){
                                                System.out.println("Winning Slab Process Error");
                                            }else {
                                                if (winningSlab.equals(oneBidPurchasedByUserInThisSameStock.getChangesBtw())){
                                                    totalNoOfBidsUsersWon = totalNoOfBidsUsersWon + Double.parseDouble(oneBidPurchasedByUserInThisSameStock.getNoOfBids());
                                                }
                                            }
                                        }
                                    }
                                }
                                Double transactionFee = biddingDraw.getTotalAmountBid()*0.1;
                                Double CurrentBTCPrice = getCurrentCoinbaseValue("https://www.coinbase.com/api/v2/assets/prices/bitcoin?base=USD");
                                double CurrentBTCPriceForPreAmount = 5.00/CurrentBTCPrice;
                                /*totalAmount = (biddingDraw.getTotalAmountBid() - transactionFee)+(18*CurrentBTCPriceForPreAmount);*/
                                totalAmount = biddingDraw.getTotalAmountBid() - transactionFee;
                                biddingDraw.setTotalTransactionFees(transactionFee);
                                overallProgressModal.setTotalAmountOfTransactionFeeForBidsPurchased(overallProgressModal.getTotalAmountOfTransactionFeeForBidsPurchased()+transactionFee);
                                for (String id : biddingDraw.getUserId()) {
                                    List<PurchaseBidsModel> allBidsPurchasedByUserInThisSameStock = purchaseBidsRepository.findAllByStockIdAndUserId(biddingDraw.getBidId(), id);
                                    WalletModel userWallet = walletRepository.findByUserId(id);

                                    for (PurchaseBidsModel oneBidPurchasedByUserInThisSameStock : allBidsPurchasedByUserInThisSameStock) {
                                        Double LossAmount = Double.parseDouble(oneBidPurchasedByUserInThisSameStock.getBiddingAmount());
                                        if (level.equals(oneBidPurchasedByUserInThisSameStock.getLevel())) {
                                            boolean changesInBetweenGuessResults = false;
                                            if (winningSlab ==null){
                                                System.out.println("Winning Slab Process Error");
                                            }else {
                                                if (winningSlab.equals(oneBidPurchasedByUserInThisSameStock.getChangesBtw())){
                                                    changesInBetweenGuessResults = true;
                                                }
                                            }

                                            if (changesInBetweenGuessResults) {
                                                oneBidPurchasedByUserInThisSameStock.setStatus("Won");
                                                double noOfBitsPurchasedOneUser = Double.parseDouble(oneBidPurchasedByUserInThisSameStock.getNoOfBids());
                                                List<String> winners = new ArrayList<>();
                                                if(biddingDraw.getWonUserId()!=null && biddingDraw.getWonUserId().size()>0){
                                                    winners = biddingDraw.getWonUserId();
                                                }
                                                winners.add(id);
                                                biddingDraw.setWonUserId(winners);
//                                    Double totalAmountOneUser = Double.parseDouble(oneBidPurchasedByUserInThisSameStock.getBiddingAmount());
                                                biddingDraw.setTotalNoOfBiddersWon(biddingDraw.getTotalNoOfBiddersWon() + 1);
                                                Double totalNoOfBidsPurchased = biddingDraw.getTotalNoOfBidsPurchased();
//                                    biddingDraw.setTotalWinningAmount(biddingDraw.getTotalWinningAmount()+ProfitOrLossAmount);
                                                UserModel user = userRepository.findById(id).orElse(null);
                                                if (user == null) {
                                                    System.out.println("There is a Transaction Issue ! UserId == " + id + " - Not Found !");
                                                } else {
                                                    TransactionModal transactionModal = new TransactionModal();
                                                    Double wonAmountOneUser = (noOfBitsPurchasedOneUser / totalNoOfBidsUsersWon) * totalAmount;
                                                    transactionModal.setTotalAmount(wonAmountOneUser);
//                                        transactionModal.setTotalAmount(ProfitOrLossAmount);
                                                    transactionModal.setCreditedUnitBtc(wonAmountOneUser);
                                                    oneBidPurchasedByUserInThisSameStock.setCreditedUnitBtc(wonAmountOneUser);
                                                    transactionModal.setStockId(biddingDraw.getBidId());
                                                    transactionModal.setStockName(biddingDraw.getStockName());
                                                    transactionModal.setStockSymbol(biddingDraw.getSymbol());
                                                    transactionModal.setUserId(user.getId());
                                                    transactionModal.setUserName(user.getUsername());
                                                    transactionModal.setTransactionMode("Wallet");
                                                    transactionModal.setTransactionType("Winning Amount");
                                                    transactionModal.setTransactionStatus("Credited");
                                                    boolean transactionStatus = MadeTransaction(transactionModal);
                                                    if (transactionStatus) {
                                                        biddingDraw.setTotalWinningAmount(biddingDraw.getTotalWinningAmount() + wonAmountOneUser);

                                                        userWallet.setDollars(userWallet.getDollars() + wonAmountOneUser);
                                                        userWallet.setTotalProfit(userWallet.getTotalProfit() + wonAmountOneUser);
                                                        userWallet.setTotalCredit(userWallet.getTotalCredit() + wonAmountOneUser);
                                                        userWallet.setTotalNumberOfTimesCredit(userWallet.getTotalNumberOfTimesCredit() + 1);
                                                        userWallet.setTotalNumberOfProfit(userWallet.getTotalNumberOfProfit() + 1);
                                                        overallProgressModal.setTotalAmountRewardedToWinners(overallProgressModal.getTotalAmountRewardedToWinners() + wonAmountOneUser);
                                                    } else {
                                                        System.out.println("There is a Transaction Issue ! UserId == " + id);
                                                    }
                                                }
                                            } else {
                                                oneBidPurchasedByUserInThisSameStock.setStatus("Lost");
                                                biddingDraw.setTotalNoOfBiddersLoss(biddingDraw.getTotalNoOfBiddersLoss() + 1);
                                                userWallet.setTotalLoss(userWallet.getTotalLoss() + LossAmount);
                                                userWallet.setTotalNumberOfLoss(userWallet.getTotalNumberOfLoss() + 1);
                                            }
                                        } else {
                                            oneBidPurchasedByUserInThisSameStock.setStatus("Lost");
                                            biddingDraw.setTotalNoOfBiddersLoss(biddingDraw.getTotalNoOfBiddersLoss() + 1);
                                            userWallet.setTotalLoss(userWallet.getTotalLoss() + LossAmount);
                                            userWallet.setTotalNumberOfLoss(userWallet.getTotalNumberOfLoss() + 1);
                                        }
                                        walletRepository.save(userWallet);
                                        purchaseBidsRepository.save(oneBidPurchasedByUserInThisSameStock);
                                        System.out.println(oneBidPurchasedByUserInThisSameStock.getUserId());
//                                    emailService.sendResultMail(oneBidPurchasedByUserInThisSameStock,biddingDraw);
                                    }
                                }
                            }
                        }
                    }
                    overallProgressModal.setTotalNoOfClosedBids(overallProgressModal.getTotalNoOfClosedBids()-1);
                    overallProgressModal.setTotalNoOfCompletedBids(overallProgressModal.getTotalNoOfCompletedBids()+1);
                    playBidRepository.save(biddingDraw);
                }
                overallProgressRepository.save(overallProgressModal);
            }
//        System.out.println(new  Date());
            return true;
        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }

    Double getCurrentCoinbaseValue(String urlString){
        try {
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            Reader streamReader = null;
            boolean Error = false;
            int status = con.getResponseCode();
            if (status > 299) {
                streamReader = new InputStreamReader(con.getErrorStream());
                Error = true;
            } else {
                streamReader = new InputStreamReader(con.getInputStream());
            }
            BufferedReader in = new BufferedReader(streamReader);
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            double CurrentPrice = 0.00;
            if (!Error){
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(String.valueOf(content));
                JSONObject data = (JSONObject) json.get("data");
                JSONObject prices = (JSONObject) data.get("prices");
                CurrentPrice = Double.parseDouble((String) prices.get("latest"));
            }
            return CurrentPrice;
        }catch (IOException | ParseException e){
            return 0.00;
        }
    }


    public boolean createPlayBid(int minutes){
        try {
            if (minutes == 3 || minutes == 5 || minutes == 7){
                Date currentServerDate = new Date();
                Date closeDate = new Date();
                Date drawDate = new Date();
                long millis = minutes*30000;
                long minutes1 = (millis / 1000)  / 60;
                int seconds = (int)((millis / 1000) % 60);
                closeDate.setMinutes(closeDate.getMinutes() + (int)minutes1);
                closeDate.setSeconds(closeDate.getSeconds() + seconds);
                drawDate.setMinutes(drawDate.getMinutes() + minutes);
                /*Bitcoin Stock*/
                PlayBidModel bitcoinModel = new PlayBidModel("Bitcoin", currentServerDate, currentServerDate, closeDate, drawDate, String.valueOf(minutes), "5", "Open Now","BTC", "assets/images/home/bitcoin.png");

                DynamicSlabModel dynamicSlabModel = dynamicSlabRepository.findByType(String.valueOf(minutes));
                if (dynamicSlabModel == null){
                    System.out.println("Dynamic Slab Not Found !");
                    return false;
                }else {
                    bitcoinModel.setDynamicSlabDetails(dynamicSlabModel);
                    return create(bitcoinModel);
                }
            }else {
                System.out.println("Bid Creation Time Exception");
                return false;
            }
        } catch (Exception e){
            System.out.println(new MessageResponse("Error info "+e));
            return false;
        }
    }

    public boolean create(PlayBidModel playBidModel) {

        try {

            if ( playBidModel.getStockName() == null
                    || playBidModel.getBiddingStartsAt() == null
                    || playBidModel.getBiddingClosesAt() == null
                    || playBidModel.getDrawTime() == null

            ) {
                return false;
            }
            List<PlayBidModel> model= playBidRepository.findAllByBidType(playBidModel.getBidType());
            long bidId = 1001L;
            if(model.size()>0){
                String bidIdFromDB = model.get(model.size()-1).getBidId();
                String[] bidIdFromDBSplit = bidIdFromDB.split("M");
                String onlyLongBidIdFromDB = bidIdFromDBSplit[1];
                bidId = Long.parseLong(onlyLongBidIdFromDB)+1;
            }
            playBidModel.setBidId(playBidModel.getBidType()+"M"+bidId);
            Double CurrentPrice = getCurrentCoinbaseValue("https://www.coinbase.com/api/v2/assets/prices/bitcoin?base=USD");
            if (CurrentPrice < 0){
                System.out.println("error");
            }else {
                playBidModel.setClosedTimeResults(CurrentPrice);
            }
            /*Jul 09*/
            List<OverallProgressModal> overallProgressModalAll = overallProgressRepository.findAll();
            OverallProgressModal overallProgressModal;
            if (overallProgressModalAll.size()==0){
                overallProgressModal = new OverallProgressModal();
                overallProgressModal.setTotalNoOfBids(1.00);
            }else {
                overallProgressModal = overallProgressModalAll.get(0);
                overallProgressModal.setTotalNoOfBids(overallProgressModal.getTotalNoOfBids()+1);
            }
            if(new Date().compareTo(playBidModel.getBiddingStartsAt())<0){
                playBidModel.setBiddingStatus("Opening Soon");
                overallProgressModal.setTotalNoOfOpeningSoonBids(overallProgressModal.getTotalNoOfOpeningSoonBids()+1);
            }else if(new Date().compareTo(playBidModel.getBiddingStartsAt())>=0){
                playBidModel.setBiddingStatus("Open Now");
                overallProgressModal.setTotalNoOfInProgressBids(overallProgressModal.getTotalNoOfInProgressBids()+1);
            }else if (new Date().compareTo(playBidModel.getBiddingClosesAt())>=0){
                playBidModel.setBiddingStatus("Closed");
                overallProgressModal.setTotalNoOfClosedBids(overallProgressModal.getTotalNoOfClosedBids()+1);
            }else if (new Date().compareTo(playBidModel.getDrawTime())>=0){
                playBidModel.setBiddingStatus("Completed");
                overallProgressModal.setTotalNoOfCompletedBids(overallProgressModal.getTotalNoOfCompletedBids()+1);
            }
            playBidModel.setTotalNoOfBiddersLoss(0.00);
            playBidModel.setTotalWinningAmount(0.00);
            playBidModel.setTotalNoOfBiddersWon(0.00);
            playBidModel.setTotalAmountBid(8*(5.00/CurrentPrice));
            playBidModel.setTotalTransactionFees(0.00);
            playBidModel.setTotalRevenue(0.00);
            Double CurrentBTCPriceForPreAmount = 5.00/CurrentPrice;
            List<Double> ArrayOfIntForFindingMaxValue = new ArrayList<Double>(Collections.nCopies(8, CurrentBTCPriceForPreAmount));
            playBidModel.setTotalNoOfAmountInSlab(ArrayOfIntForFindingMaxValue);
            playBidRepository.save(playBidModel);
            overallProgressRepository.save(overallProgressModal);
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

    }
}
